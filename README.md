# fast-wms
WMS-T / MVT server based on fast-api.
### Build
```shell
make build
```
### Running
A small sample dataset is included for testing in `tests/data/data.nc`
```shell
make run
```

### Run tests
Run the server as above, then in another shell with requirements from `dev-requirements.txt` run
```shell
pytest -s tests/test.py
```
