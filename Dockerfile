FROM debian:bookworm-slim
RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests --allow-unauthenticated -y \
    dumb-init \
    python3-gdal \
    python3.11-venv \
    && rm -rf /var/lib/apt/lists/*
RUN mkdir /data
WORKDIR /opt/server

RUN python3 -m venv --system-site-packages venv
COPY requirements.txt requirements.txt
RUN venv/bin/pip install -r requirements.txt && venv/bin/pip cache purge
COPY fast_wms fast_wms

# RUN gcc -o /opt/server/lib/libvector.so -shared -O2 fast_wms/vector.c 
ENTRYPOINT [ "dumb-init", "--" ]
CMD ["venv/bin/uvicorn", "fast_wms.app:app", "--host", "0.0.0.0", "--port", "8000"]
