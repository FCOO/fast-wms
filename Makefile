.PHONY: build run
build:
	docker build -t fast-wms-public:latest .

run:
	docker run -ti \
	--name fast-wms \
	-e POOL_SIZE=4 \
	-v ./tests/data:/data \
	-e PREFIX=/webmap/v3 \
	--rm -p 8000:8000 -v ./fast_wms/:/opt/server/fast_wms \
	fast-wms-public venv/bin/uvicorn --host 0.0.0.0 --port 8000 --reload fast_wms.app:app

