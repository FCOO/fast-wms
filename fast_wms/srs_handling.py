"""
Code to (mostly) guess the SRS / CRS of a multidimensional (netCDF) dataset
"""

from dataclasses import dataclass
import typing as T
import time

from osgeo import osr
import netCDF4 as nc
import numpy as np

epsg4326 = osr.SpatialReference()
epsg4326.ImportFromEPSG(4326)
wkt_4326 = epsg4326.ExportToWkt()
epsg3413 = osr.SpatialReference()
epsg3413.ImportFromEPSG(3413)
epsg3857 = osr.SpatialReference()
epsg3857.ImportFromEPSG(3857)
# DMI: https://confluence.govcloud.dk/pages/viewpage.action?pageId=76153193
dmi_nea = osr.SpatialReference()
dmi_nea.ImportFromProj4(
    "+proj=ob_tran +o_proj=longlat +o_lon_p=0 +o_lat_p=40 +lon_0=26.5 +R=6367470 +no_defs"
)
wkt_dmi_nea = dmi_nea.ExportToWkt()
dmi_igb = osr.SpatialReference()
dmi_igb.ImportFromProj4(
    "+proj=ob_tran +o_proj=longlat +o_lon_p=0 +o_lat_p=34.07 +lon_0=-36.65 +R=6367470 +no_defs"
)
wkt_dmi_igb = dmi_igb.ExportToWkt()

dmi_dini = osr.SpatialReference()
dmi_dini.ImportFromProj4(
    "+proj=lcc +lat_1=55.5 +lat_0=55.5 +lon_0=352 +x_0=1527524.88192815 +y_0=1588680.97691483 +R=6371229 +units=m +no_defs"
)
wkt_dmi_dini = dmi_dini.ExportToWkt()

dmi_ig = osr.SpatialReference()
dmi_ig.ImportFromProj4(
    "+proj=lcc +lat_1=72.000002 +lat_0=72.000002 +lon_0=323.999999 +x_0=1337999.57961295 +y_0=1588680.97691483 +R=6371229 +units=m +no_defs"
)
wkt_dmi_ig = dmi_ig.ExportToWkt()

SRS = {
    "4326": epsg4326,
    "3413": epsg3413,
    "3857": epsg3857,
}


@dataclass
class Rect:
    x1: float
    x2: float
    y1: float
    y2: float


# Max bounds
SRS_BBOX = {
    "3857": Rect(-20037508.342789244, 20037508.342789244, -20048966.1, 20048966.1),
    "4326": Rect(-180, -90, 180, 90),
}

# For caching SRS dynamically created
SRS_CACHE = {}


def identify_srs(ds: nc.Dataset) -> T.Tuple[osr.SpatialReference, str, int, int, bool]:
    """
    Heuristics to determine SRS (best guess)
    Default, if not a 'speciel case', is EPSG:4326 (WGS84)
    Returns:
        spatial_ref, wkt, idx_x, idx_y, do_densify_bbox
    """
    t1 = time.perf_counter()
    densify = False
    idx_x = 0
    idx_y = 1
    if "rotated_pole" in ds.variables:
        np_lat = ds.variables["rotated_pole"].grid_north_pole_latitude
        np_lon = ds.variables["rotated_pole"].grid_north_pole_longitude
        if np_lat == 40.0 and np.isclose(np_lon % 180, 26.5 % 180):
            src_srs = dmi_nea
            src_wkt = wkt_dmi_nea
        elif np_lat == 34.07 and np.isclose(np_lon % 180, -36.65 % 180):
            src_srs = dmi_igb
            src_wkt = wkt_dmi_igb
        else:
            p4_string = f"+proj=ob_tran +o_proj=longlat +o_lon_p=0 +o_lat_p={np_lat} +lon_0={np_lon + 180} +R=6367470 +no_defs"
            if p4_string in SRS_CACHE:
                src_srs, src_wkt = SRS_CACHE[p4_string]
            else:
                src_srs = osr.SpatialReference()
                src_srs.ImportFromProj4(p4_string)
                src_wkt = src_srs.ExportToWkt()
                SRS_CACHE[p4_string] = (src_srs, src_wkt)
        # We only need to densify when at large zoom
        densify = True
    elif "crs" in ds.variables:
        densify = True
        crs = ds.variables["crs"]
        if getattr(crs, "grid_mapping_name", "") == "lambert_conformal_conic":
            sp = getattr(crs, "standard_parallel", None)
            er = getattr(crs, "earth_radius", None)
            lcm = getattr(crs, "longitude_of_central_meridian", None)
            lpo = getattr(crs, "latitude_of_projection_origin", None)
            false_easting = getattr(crs, "false_easting", None)
            false_northing = getattr(crs, "false_northing", None)
            idx_y = 1
            idx_x = 0
            if (
                sp == 55.5
                and er == 6371229.0
                and (lcm % 360) == (352.0 % 360)
                and lpo == 55.5
                and abs(false_easting - 1527524.881928148) < 1e-2
                and abs(false_northing - 1588680.976914835) < 1e-2
            ):
                src_srs = dmi_dini
                src_wkt = wkt_dmi_dini
            elif (
                sp == 72.000002
                and er == 6371229.0
                and (lcm % 360) == (323.999999 % 360)
                and lpo == 72.000002
                and abs(false_easting - 1337999.57961295) < 1e-2
                and abs(false_northing - 1588000.56888229) < 1e-2
            ):
                src_srs = dmi_ig
                src_wkt = wkt_dmi_ig
            elif (
                sp is not None
                and er is not None
                and lcm is not None
                and false_easting is not None
                and false_northing is not None
            ):
                # 1SP variant lat1 = lat2
                p4_string = f"+proj=lcc +lat_1={sp} +lat_0={lpo} +lon_0={lcm} +x_0={false_easting} +y_0={false_northing} +R={er} +units=m +no_defs"
                if p4_string in SRS_CACHE:
                    src_srs, src_wkt = SRS_CACHE[p4_string]
                else:
                    src_srs = osr.SpatialReference()
                    src_srs.ImportFromProj4(p4_string)
                    src_wkt = src_srs.ExportToWkt()
                    SRS_CACHE[p4_string] = (src_srs, src_wkt)
            else:
                raise ValueError("Could not instantiate LCC projection for source")
        elif hasattr(crs, "proj4_string"):
            # E.g. DMI icechart
            p4_string = ds.variables["crs"].proj4_string
            if p4_string in SRS_CACHE:
                src_srs, src_wkt = SRS_CACHE[p4_string]
            else:
                src_srs = osr.SpatialReference()
                src_srs.ImportFromProj4(p4_string)
                src_wkt = src_srs.ExportToWkt()
                SRS_CACHE[p4_string] = (src_srs, src_wkt)
        else:
            raise ValueError("Could not instantiate LCC projection for source")
    else:
        src_srs = epsg4326
        src_wkt = wkt_4326
        # The order of coords when transforming to EPSG:4326 is (lat, lon) i.e. 'y,x'
        idx_y = 0
        idx_x = 1
        densify = False
    t2 = time.perf_counter()
    # print("SRS: ", t2 - t1)
    return src_srs, src_wkt, idx_x, idx_y, densify
