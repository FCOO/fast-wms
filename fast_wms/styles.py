"""
Code for handling styles etc
"""

from pathlib import Path
import json
import typing as T
from dataclasses import dataclass

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.cm import get_cmap
from matplotlib.colors import Normalize
from matplotlib.colors import from_levels_and_colors
from matplotlib.figure import Figure
import numpy as np

# Only for ice_colors (maybe not needed)
from osgeo import gdal

with (Path(__file__).parent / "colors.json").open() as f:
    MP_COLORS = json.load(f)


class Colormap:
    """
    Colormap with more or less same functionality as a matplotlib colormap
    """

    black_val = None
    transparent_val = None

    def __init__(
        self,
        levels: T.Iterable[float],
        colors: T.Iterable[T.Iterable[float]],
        add_black: bool = True,
        add_transparent: bool = True,
        extend: str = "max",
    ):
        if levels is not None:
            assert len(levels) <= 256 - int(add_black) - int(add_transparent)
            self.levels = np.array(levels)
            dl = self.levels[1:] - self.levels[0:-1]
            self.linear = (dl == dl[0]).all()
            self.mp_cmap, self.mp_norm = from_levels_and_colors(levels, colors, extend)
        else:
            self.levels = None
            self.linear = False
        self.extend = extend
        self.pallette = []
        # Check if scale is linear

        for n, row in enumerate(colors):
            col = [int(val * 255) for val in row]
            if len(col) == 3:
                col += [255]
            self.pallette.extend(col)
        if add_transparent:
            n += 1
            # Transparent
            self.pallette.extend([0, 0, 0, 0])
            self.transparent_val = n
        if add_black:
            n += 1
            # black
            self.pallette.extend([0, 0, 0, 255])
            self.black_val = n
        self.n_colors = n + 1
        self.base_colors = colors

    def array_to_colors(
        self, inp: np.ndarray, nd_mask: np.ndarray = None
    ) -> np.ndarray:
        # For making ice charts work - transparent at zero level
        # Choose right endpoint to be included in interval of color
        right = True  # if self.extend in ("min", "both") else False
        scaled = np.digitize(inp, self.levels, right=right)
        if self.extend in ("max", "neither"):
            # digitize reports bin number + 1
            # This should be mapped properly to color array, which in some cases may be larger than number of bins
            scaled -= 1
        # Everything which aint nodata (even below min or above max) gets colored
        # This can be problematic, e.g for rain close to 0, which should really be nothing
        # Summa summarum: Handle "extend" properly
        transparent_mask = np.zeros(scaled.shape, dtype=bool)
        if self.extend in ("both", "min"):
            scaled[scaled < 0] = 0
        else:
            # If we're bounded below, handle lowest level (e.g. 0)
            # Which is NOT included in interval
            cut = inp == self.levels[0]
            scaled[cut] = 0
            # Rest is transparent
            transparent_mask[scaled < 0] = True
        mx = len(self.base_colors) - 1
        # print(mx, scaled.max(), scaled.min())
        if self.extend in ("both", "max"):
            scaled[scaled > mx] = mx
        else:
            # Right is True, so right endpoint should be included
            transparent_mask[scaled > mx] = True

        arr = scaled.astype(np.uint8)

        if nd_mask is not None:
            transparent_mask |= nd_mask
            arr[transparent_mask] = self.transparent_val
        # print(transparent_mask.sum(), "NT", inp.max(), inp.min())
        return arr

    @classmethod
    def from_mp_colors(cls, mp_def) -> "Colormap":
        return cls(**mp_def)

    @classmethod
    def black_vector_colormap(
        cls,
        levels: T.Iterable[float],
        border_color: T.Iterable[int] = (0.95, 0.95, 0.95, 1.0),
    ) -> "Colormap":
        cmap = cls(
            levels,
            # black, white, trans
            [(0.2, 0.2, 0.2, 1.0), border_color, (0, 0, 0, 0)],
            add_black=False,
            add_transparent=False,
        )
        # Really the border val
        cmap.black_val = 0
        cmap.transparent_val = 2
        return cmap


def get_ice_colors() -> Colormap:
    cols = gdal.ColorTable()
    cols.CreateColorRamp(0, (0, 0, 255, 255), 128, (0, 255, 100, 255))
    cols.CreateColorRamp(128, (0, 255, 100, 255), 254, (255, 10, 0, 255))
    levels = np.linspace(0, 6, 255)
    col_arr = np.zeros((255, 4), dtype=np.uint8)
    for i in range(255):
        col_arr[i] = cols.GetColorEntry(i)
    col_arr = col_arr / 255.0
    return Colormap(levels, col_arr, add_black=False, add_transparent=True)


@dataclass
class Style:
    style_str: str  # The original style string
    colormap: Colormap = None
    extra_nd_val: float = None
    is_vector: bool = False
    # BW vector, only direction
    only_dir: bool = False
    plot_method: str = None
    vector_spacing: int = 0
    gaussian_filter: int = 0


COLORS = {key: Colormap.from_mp_colors(val) for key, val in MP_COLORS.items()}
# Some special styles
STYLES = {
    "ice": Style("ice", get_ice_colors(), 0),
    "vector_current": Style(
        "vector_current",
        Colormap.from_mp_colors(MP_COLORS["Current_kn_WGYR_11colors"]),
        is_vector=True,
    ),
}


def parse_style(style_str: str, cmap_str: str = None) -> Style:
    """
    Parse a style_str which can be an explicit predefined name
    or a 'instructions' separated by ; e.g:
    plot_method=contourf;legend=Hs_m_GBP_12colors_denmark
    """
    if style_str in STYLES:
        return STYLES[style_str]
    # Handle style, e.g. plot_method=contourf;legend=Hs_m_GBP_12colors_denmark
    style_def = {}
    for kv in style_str.split(";"):
        key, val = kv.split("=")
        style_def[key] = val
    cmap = None
    if cmap_str:
        cmap = COLORS.get(cmap_str)
    elif "legend" in style_def:
        cmap = COLORS.get(style_def["legend"])
    # These two should work without colormap
    if cmap is None and style_def["plot_method"] not in (
        "black_vector",
        "black_arrowbarbs",
        "black_barbs",
    ):
        # Come up with a stupid colormap with 0 as min
        raise ValueError("Color not defined")
    style = Style(style_str, cmap)
    if (
        style_def["plot_method"] == "black_vector"
        or "quiver" in style_def["plot_method"]
        or "barbs" in style_def["plot_method"]
    ):
        style.is_vector = True
    style.plot_method = style_def["plot_method"]
    style.vector_spacing = int(style_def.get("vector_spacing", 0))
    style.gaussian_filter = int(style_def.get("gaussian_filter", 0))
    return style
