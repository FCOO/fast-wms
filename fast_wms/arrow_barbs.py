"""
Support for arrow heads on barb plots. Subclasses matplotlib Barbs class
and overrides _make_barbs method.
"""

from typing import Sequence
from functools import lru_cache

import numpy as np
from numpy import ma
import matplotlib.transforms as transforms
from matplotlib.patches import CirclePolygon
from matplotlib.quiver import Barbs


def arrow_barbs(ax, *args, **kw):
    """Make arrow barbs."""
    b = ArrowBarbs(ax, *args, **kw)
    ax.add_collection(b, autolim=True)
    ax.autoscale_view()
    return b


@lru_cache(maxsize=10)
def get_circle_verts(radius: float) -> np.ndarray:
    """
    A slightly faster cached version
    """
    return CirclePolygon((0, 0), radius=radius).get_verts()


class ArrowBarbs(Barbs):
    """
    Subclass of mpl.quiver.Barbs with support for arrow heads
    """

    def _make_barbs(
        self,
        u: Sequence[float],
        v: Sequence[float],
        nflags: Sequence[int],
        nbarbs: Sequence[int],
        half_barb: Sequence[bool],
        empty_flag: Sequence[bool],
        length: int,
        pivot: str,
        sizes: dict[str, float],
        fill_empty: bool,
        flip: Sequence[bool],
    ) -> list[np.ndarray | list[tuple[float, float]]]:
        """
        Overrides the _make_barbs method.
        Draws an arrow with a head as well as the barbs.
        Modification of the _make_barbs method from mpl.quiver.Barbs
        :Returns list of polygon coordinates
        """
        # These control the spacing and size of barb elements relative to the
        # length of the shaft
        spacing = length * sizes.get("spacing", 0.125)
        full_height = length * sizes.get("height", 0.4)
        full_width = length * sizes.get("width", 0.25)
        empty_rad = length * sizes.get("emptybarb", 0.15)
        width = length * sizes.get("width", 0.05)
        headwidth = width * (sizes.get("headwidth", 1.0) - 1)
        headlength = width * sizes.get("headlength", 1.0)
        headaxislength = width * sizes.get("headaxislength", 1.0)

        hwidth = 0.5 * width
        headaxislength = headlength - headaxislength
        spacing = max(spacing, width * 1.1)

        pivot_points = dict(tip=0.0, middle=-length / 2.0, tail=-length)

        endx = 0.0
        endy = pivot_points[pivot.lower()]

        angles = -(ma.arctan2(v, u) + np.pi / 2)

        # Low magnitude drawn as circles
        circ = get_circle_verts(empty_rad)

        if fill_empty:
            empty_barb = circ
        else:
            empty_barb = np.concatenate((circ, circ[::-1]))

        barb_list = []
        for index, angle in enumerate(angles):
            # Circle for weak magnitudes
            if empty_flag[index]:
                barb_list.append(empty_barb)
                continue

            poly_verts = [
                (endx, endy),
                (endx + headwidth / 2, endy + headaxislength),
                (endx - hwidth, endy - headlength + headaxislength),
                (endx - headwidth / 2 - width, endy + headaxislength),
                (endx - width, endy),
                (endx - width, endy + length),
            ]

            offset = length

            # Flip individual barbs
            barb_height = -full_height if flip[index] else full_height

            # Add vertices for flags
            for _ in range(nflags[index]):
                # The spacing that works for the barbs is a little to much for
                # the flags, but this only occurs when we have more than 1
                # flag.
                poly_verts.extend(
                    [
                        [endx, endy + offset],
                        [endx + barb_height + hwidth, endy - full_width / 2 + offset],
                        [endx, endy - full_width + offset],
                    ]
                )

                offset -= full_width + spacing / 2.0

            # Add vertices for barbs
            for _ in range(nbarbs[index]):
                poly_verts.extend(
                    [
                        (endx, endy + offset),
                        (endx + barb_height, endy + offset + full_width / 2),
                        (
                            endx + barb_height + hwidth,
                            endy + offset + full_width / 2 - hwidth,
                        ),
                        (endx + barb_height, endy + offset + full_width / 2 - width),
                        (endx, endy + offset - width),
                    ]
                )

                offset -= spacing

            # Handle half barbs
            if half_barb[index]:
                if offset == length:
                    poly_verts.append((endx, endy + offset))
                    offset -= 1.5 * spacing
                poly_verts.extend(
                    [
                        (endx, endy + offset),
                        (endx + barb_height / 2, endy + offset + full_width / 4),
                        (
                            endx + barb_height / 2 + hwidth,
                            endy + offset + full_width / 4 - hwidth,
                        ),
                        (
                            endx + barb_height / 2,
                            endy + offset + full_width / 4 - width,
                        ),
                        (endx, endy + offset - width),
                    ]
                )

            # Finally, rotate the arrow barb
            poly_verts = transforms.Affine2D().rotate(-angle).transform(poly_verts)
            barb_list.append(poly_verts)

        return barb_list
