import typing as T
import datetime
import json
import os
from pathlib import Path

import netCDF4 as nc
import numpy as np
from osgeo import osr

from .vnetcdf import ncv_wrapper
from .utils import get_ncmetadata, get_time_values
from . import srs_handling

MIN_WIDTH = 0
MAX_WIDTH = 8192
MIN_HEIGHT = 0
MAX_HEIGHT = 8192
DEFAULT_CRS = "EPSG:4326"
WMS_VERSION = "1.3.0"
SUPPORTED_CRS = ["EPSG:4326", "EPSG:3857"]  # , 'EPSG:3413', 'EPSG:3031']

with (Path(__file__).parent / "styles.json").open() as f:
    LAYER_TO_STYLENAME = json.load(f)


DEFAULT_TEMPLATE = """<?xml version='1.0' encoding="UTF-8"?>
<WMS_Capabilities version="1.3.0" xmlns="http://www.opengis.net/wms"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:py="http://genshi.edgewall.org/"
  xsi:schemaLocation="http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd">

<Service>
  <Name>WMS</Name>
  <Title>WMS server for {name}</Title>
  <MaxWidth>{max_width}</MaxWidth>
  <MaxHeight>{max_height}</MaxHeight>
  <OnlineResource xlink:href="{location}"></OnlineResource>
</Service>

<Capability>
  <Request>
    <GetCapabilities>
      <Format>text/xml</Format>
      <DCPType>
        <HTTP>
          <Get><OnlineResource xlink:href="{location}"></OnlineResource></Get>
        </HTTP>
      </DCPType>
    </GetCapabilities>

    <GetMap>
      <Format>image/png</Format>
      <DCPType>
        <HTTP>
          <Get><OnlineResource xlink:href="{location}"></OnlineResource></Get>
        </HTTP>
      </DCPType>
    </GetMap>
  </Request>
  <Exception>
    <Format>XML</Format>
  </Exception>
  <Layer>
    <Title>WMS server for {name}</Title>
    {crs_list}
    <EX_GeographicBoundingBox>
        <westBoundLongitude>{minlon}</westBoundLongitude>
        <eastBoundLongitude>{maxlon}</eastBoundLongitude>
        <southBoundLatitude>{minlat}</southBoundLatitude>
        <northBoundLatitude>{maxlat}</northBoundLatitude>
    </EX_GeographicBoundingBox>
    <BoundingBox CRS="{default_crs}" minx="{minlat}" miny="{minlon}" maxx="{maxlat}" maxy="{maxlon}"/>
    {layers}
  </Layer>
</Capability>
</WMS_Capabilities>"""

LAYER_TEMPLATE = """<Layer>
      <Name>{name}</Name>
      <Title>{title}</Title>
      <Abstract>{abstract}</Abstract>
      <EX_GeographicBoundingBox>
        <westBoundLongitude>{minlon}</westBoundLongitude>
        <eastBoundLongitude>{maxlon}</eastBoundLongitude>
        <southBoundLatitude>{minlat}</southBoundLatitude>
        <northBoundLatitude>{maxlat}</northBoundLatitude>
      </EX_GeographicBoundingBox>
      <BoundingBox CRS="{default_crs}" minx="{minlat}" miny="{minlon}" maxx="{maxlat}" maxy="{maxlon}"/>
      {dim_time}
      {dim_z}
      {styles}      
    </Layer>"""


LEGEND_TMPL = """
<LegendURL width="250" height="40">
    <Format>image/png</Format>
    <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
    xlink:type="simple"
    xlink:href="{location}?REQUEST=GetColorbar&amp;STYLES=horizontal%2Cnolabel&amp;CMAP={legend}&amp;SIZE=250" />
</LegendURL>
"""


STYLE_TMPL = """<Style>
    <Name>{style_str}</Name>
    <Title>Style {style_str} for {name}</Title>
    <LegendURL width="250" height="40">
        <Format>image/png</Format>
        <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink"
            xlink:type="simple"
            xlink:href="{location}?REQUEST=GetColorbar&amp;STYLES=horizontal%2Cnolabel&amp;CMAP={legend}&amp;SIZE=250" />
    </LegendURL>
</Style>"""

# TODO:


DIM_TIME_TMPL = """<Dimension name="time" units="ISO8601" default="{nearest_time}" nearestValue="1">
{times}
</Dimension>"""

DIM_Z_TMPL = """<Dimension name="elevation" units="{units}" default="{default_z}" multipleValues="0" nearestValue="0">
{zs}
</Dimension>"""


def generate_style(style: T.Dict[str, T.Any], name: str, location: str) -> str:
    """
    Generate style element
    """
    style_list = []
    for key, value in style.items():
        item = key + "=" + value
        style_list.append(item)
    style_str = ";".join(style_list)
    return STYLE_TMPL.format(
        style_str=style_str, name=name, location=location, legend=style["legend"]
    )


def get_default_style(ncfile: str, ncvars: T.Sequence[str]):
    if len(ncvars) not in (1, 2):
        return None
    with nc.Dataset(ncfile) as ds:
        if len(ncvars) == 2:
            # Vectors
            sn1 = getattr(ds.variables[ncvars[0]], "standard_name", "")
            sn2 = getattr(ds.variables[ncvars[1]], "standard_name", "")
            prefix, postfix = sn1.split("eastward_", 1)[:]
            assert sn2 == prefix + "northward_" + postfix
            # E.g. 'wind' or 'current'
            sn = postfix
        else:
            sn = getattr(ds.variables[ncvars[0]], "standard_name", None)
        if sn in LAYER_TO_STYLENAME:
            if len(ncvars) == 2:
                for style in LAYER_TO_STYLENAME[sn]:
                    if style["plot_method"] != "contourf":
                        break
            else:
                style = LAYER_TO_STYLENAME[sn][0]
            style_list = []
            for key, value in style.items():
                item = key + "=" + value
                style_list.append(item)
            style_str = ";".join(style_list)
            return style_str
    return None


def layer_xml(layer: T.Dict[str, T.Any], location: str) -> str:
    context = {
        "name": layer["name"],
        "title": layer["title"],
        "abstract": layer["abstract"],
        "minlon": layer["bbox"][0],
        "maxlon": layer["bbox"][2],
        "minlat": layer["bbox"][1],
        "maxlat": layer["bbox"][3],
        "default_crs": DEFAULT_CRS,
    }
    times = ",".join(layer["times"])
    context["dim_time"] = DIM_TIME_TMPL.format(
        nearest_time=layer["nearest_time"], times=times
    )
    # Vertical
    if vertical := layer.get("vertical"):
        context["dim_z"] = DIM_Z_TMPL.format(**vertical)
    else:
        context["dim_z"] = ""
    context["styles"] = "\n".join(
        generate_style(style, name=layer["name"], location=location)
        for style in layer["styles"]
    )
    return LAYER_TEMPLATE.format(**context)


def build_layers(
    ncfile: str,
    supported_styles: T.Dict[str, T.Any] = LAYER_TO_STYLENAME,
    varnames: T.Iterable[str] = None,
    add_vectorfields: bool = True,
) -> T.Tuple[T.Dict[str, T.Any], str, str]:
    # Return list of lyr meta, epoch, last_modified
    global_times = None
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    if ncfile.endswith(".ncv"):
        ncfile, global_times, bbox = ncv_wrapper(
            ncfile,
            # Means we last file (newest)
            # Also epoch and last_modified will be relative to this one
            # NOTE: Consider using last_modified from vnetcdf file
            time_inp=None,
            return_additonal_metadata=True,
        )
        # Find nearest time in this case
        diffs = np.fabs([(now - t).total_seconds() for t in global_times])
        tidx = np.argmin(diffs)
        global_times = [t.strftime("%Y-%m-%dT%H:%M:%SZ") for t in global_times]
        nearest_time = global_times[tidx]
    IGNORED_DTYPES = ["|S1"]  # TODO: Maybe more to come

    global_epoch = None
    last_modified = datetime.datetime.fromtimestamp(
        Path(ncfile).stat().st_mtime
    ).strftime("%Y-%m-%dT%H:%M:%SZ")
    with nc.Dataset(ncfile) as ds:
        src_srs, _, _, _, _ = srs_handling.identify_srs(ds)
        transform = None
        if src_srs != srs_handling.epsg4326:
            # Transform
            # print("Establising transform")
            transform = osr.CoordinateTransformation(src_srs, srs_handling.epsg4326)
        global_epoch = getattr(ds, "epoch", None)
        if varnames is None:
            # Use all (mostly)
            # Do not display grids containing these data types
            grids = [
                grid
                for grid in ds.variables.values()
                if (
                    grid.name not in ds.dimensions
                    and str(grid.dtype) not in IGNORED_DTYPES
                    and "time" in grid.dimensions
                    and grid.ndim > 2
                    and grid.shape[0] > 0
                )
            ]
        else:
            grids = [ds.variables[varname] for varname in varnames]
        # Store information for regular layers
        layers = []
        dims_cache = {}
        for var in grids:
            # Style information
            standard_name = getattr(var, "standard_name", None)
            styles = []
            if standard_name is not None:
                styles = supported_styles.get(standard_name, [])

            # Spatial information
            key = var.dimensions
            if key in dims_cache:
                extents = dims_cache[key]
            else:
                res = get_ncmetadata(ds, var.dimensions)
                dims_cache[key] = res
                extents = res
            # BTW: Lots of this can be cached too
            # TODO: Optimize a bit here
            # Assuming that dimensions are time, (elevation), lat, lon
            # TODO: We need to take idx_lat, idx_lon into consideration here (in src system)
            minx, maxx = extents[-1][:2]
            miny, maxy = extents[-2][:2]
            bbox = [float(minx), float(miny), float(maxx), float(maxy)]
            if transform is not None:
                pts = transform.TransformBounds(bbox[0], bbox[1], bbox[2], bbox[3], 8)
                # Order is (lat, lon) in epsg4326 when transforming
                bbox_4326 = [
                    pts[1],
                    pts[0],
                    pts[3],
                    pts[2],
                ]
                # print("YEAH", pts, bbox_4326)
            else:
                bbox_4326 = bbox
            tvar = ds.variables[var.dimensions[0]]
            times = None
            if not global_times:
                calendar = getattr(tvar, "calendar", "standard")
                tidx = nc.date2index(now, tvar, select="nearest", calendar=calendar)
                # Time information (atm assuming linear spacing)
                times = [
                    t.strftime("%Y-%m-%dT%H:%M:%SZ") for t in get_time_values(tvar)
                ]
                nearest_time = times[tidx]

            if var.ndim == 4:
                # Second dim is vertical
                z_dim = ds.variables[var.dimensions[1]]
                vals = np.ma.getdata(z_dim[:])
                vertical = {
                    "units": getattr(z_dim, "units", ""),
                    "default_z": vals[0],
                    "zs": ",".join(["%.1f" % x for x in vals]),
                }
                if pos := getattr(z_dim, "positive", None):
                    vertical["positive"] = pos
            else:
                vertical = None
            title = getattr(var, "long_name", var.name)
            if title != var.name:
                title += " - " + var.name
            layer = {
                "name": var.name,
                "title": title,
                "abstract": getattr(var, "history", ""),
                "styles": styles,
                "bbox": bbox_4326,
                "vertical": vertical,
                "times": global_times or times,
                "standard_name": standard_name,
                "long_name": getattr(var, "long_name", ""),
                "units": getattr(var, "units", ""),
                "nearest_time": nearest_time,
            }
            layers.append(layer)
            if (
                standard_name is not None
                and "eastward" in standard_name
                and add_vectorfields
            ):
                # Handle arrow layers (eastward + northward)
                prefix, postfix = standard_name.split("eastward_", 1)[:]
                search_standard_name = prefix + "northward_" + postfix
                for v_var in grids:

                    v_standard_name = getattr(v_var, "standard_name", None)
                    if search_standard_name == v_standard_name:
                        key = v_var.dimensions
                        if key in dims_cache:
                            res = dims_cache[key]
                        else:
                            res = get_ncmetadata(ds, v_var.dimensions)
                            dims_cache[key] = res
                        v_extents = res
                        minx, maxx = v_extents[-1][:2]
                        miny, maxy = v_extents[-2][:2]
                        v_bbox = [minx, miny, maxx, maxy]
                        # Not same 'dataset' (possibly something nested)
                        if v_bbox != bbox or extents[0] != v_extents[0]:
                            continue
                        styles = [
                            st
                            for st in supported_styles.get(postfix, [])
                            if st["plot_method"] != "contourf"
                        ]
                        lyrname = ":".join([var.name, v_var.name])
                        layer = {
                            "name": lyrname,
                            "title": postfix + " - " + lyrname,
                            "abstract": getattr(var, "history", ""),
                            "styles": styles,
                            "bbox": bbox_4326,
                            "vertical": vertical,
                            "times": global_times or times,
                            "nearest_time": nearest_time,
                        }
                        layers.append(layer)

    return layers, global_epoch, last_modified


def get_metadata(
    ncfile: str,
    layers: T.Iterable[str],
    items: T.Iterable[str] = ("units", "long_name", "bounds", "time"),
) -> T.Dict[str, T.Any]:
    """
    Get metadata - pretty much a json version of capabilities
    """
    # Translate items to what we use in build_layers
    translations = {
        "time": "times",
        "bounds": "bbox",
        "levels": "vertical",
    }
    if not items:
        items = ("units", "long_name", "bounds", "time")
    # Use a set
    items = set(items)
    out = {}
    lyr_meta, epoch, last_modified = build_layers(
        ncfile, varnames=layers, add_vectorfields=False
    )
    if "last_modified" in items:
        out["last_modified"] = last_modified
        items.remove("last_modified")
    if "epoch" in items:
        out["epoch"] = epoch
        items.remove("epoch")

    for name, lyr in zip(layers, lyr_meta):
        out[name] = {}
        for key in items:
            tkey = translations.get(key, key)
            val = lyr.get(tkey)
            if tkey == "vertical" and val is not None:
                val = {
                    "values": [float(x) for x in val["zs"].split(",")],
                    "units": val["units"],
                    "positive": val.get("positive"),
                }
            if val is not None:
                out[name][key] = val
    return out


def get_capability_xml(ncfile: str, location: str) -> str:
    """
    Generate full capability doc
    """
    layers, _, _ = build_layers(ncfile)
    # TODO: Extend bbox by looping over layers
    # Consider what the standard is for a 'total' bbox
    context = {
        "name": os.path.basename(ncfile),
        "max_width": MAX_WIDTH,
        "max_height": MAX_HEIGHT,
        "minlon": layers[0]["bbox"][0],
        "maxlon": layers[0]["bbox"][2],
        "minlat": layers[0]["bbox"][1],
        "maxlat": layers[0]["bbox"][3],
        "default_crs": DEFAULT_CRS,
        "location": location,
    }
    context["crs_list"] = "\n".join([f"<CRS>{crs}</CRS>" for crs in SUPPORTED_CRS])
    context["layers"] = "\n".join(layer_xml(lyr, location) for lyr in layers)
    return DEFAULT_TEMPLATE.format(**context)
