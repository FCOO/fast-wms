import typing as T
import datetime
import time

import numpy as np
import netCDF4 as nc

# from .vnetcdf import ncv_wrapper


def get_ndval(var: nc.Variable) -> float:
    """
    Heuristic to settle actual no data value
    """
    # Missing value may be out of scope and ignored
    if hasattr(var, "missing_value"):
        nd_val = var.missing_value
    elif hasattr(var, "_FillValue"):
        nd_val = var._FillValue
    else:
        # TODO: Use value in range of dtype
        var.set_auto_mask(False)
        nd_val = -9999
    # print(var.name, nd_val, "SETTING")
    return nd_val


def get_time_values(
    tvar: nc.Variable, min_max_only: bool = False, as_pydatetimes: bool = False
) -> list[datetime.datetime]:
    # Seems to be the right way even though standard is off by 2 days
    calendar = getattr(tvar, "calendar", "standard")

    if min_max_only:
        nctimes = []
        for idx in (0, -1):
            nctimes.append(
                nc.num2date(tvar[idx].data, units=tvar.units, calendar=calendar)
            )

    else:
        nctimes = nc.num2date(tvar[:].data, units=tvar.units, calendar=calendar)
    if as_pydatetimes:
        return [
            datetime.datetime.fromisoformat(dt_nc.isoformat()).replace(
                tzinfo=datetime.timezone.utc
            )
            for dt_nc in nctimes
        ]
    return nctimes


def get_ncmetadata(
    ds: nc.Dataset, dims: T.Iterable[str]
) -> list[tuple[float, float, float, float]]:
    """
    Get metadata for dimensions (could be generalized)
    """
    # NOTE: This function takes around 0.7 - 1.2 ms
    # TODO: Consider what this actually should return, e.g indexing vars instead of dimensions. What is needed?
    t1 = time.perf_counter()
    # We assume that time is first dim
    # And we always want time values to be in seconds (since epoch) with first value 0 (i.e. epoch is first value)
    extents = []
    for dim in dims:
        if dim in ds.variables:
            _var = ds.variables[dim]
        else:
            # Find indexing variable
            _var = None
            for tv in ds.variables.values():
                if len(tv.dimensions) == 1 and tv.dimensions[0] == dim:
                    _var = tv
                    break
            if _var is None:
                raise ValueError(
                    f"Unable to find indexing variable for dimension {dim}"
                )
        # extent is (min, max, stepsize, array_size)
        if _var.ndim == 1:
            mx = _var[-1].data
            mi = _var[0].data
            dv = (mx - mi) / (_var.shape[0] - 1)
        else:
            # Only min and max are relevant in this case
            mx = np.ma.max(_var[:])
            mi = np.ma.min(_var[:])
            dv = None
        extents.append((mi, mx, dv, _var.shape[0]))
    t2 = time.perf_counter()
    return extents
