"""
Validate WMS request
"""

import typing as T
from dataclasses import dataclass, fields
import datetime
import json

WMS_VERSION = "1.3.0"

MIN_WIDTH = 0
MAX_WIDTH = 8192
MIN_HEIGHT = 0
MAX_HEIGHT = 8192

DEFAULT_CRS = "EPSG:4326"

SUPPORTED_CRS = ["EPSG:4326", "EPSG:3857"]  # , 'EPSG:3413', 'EPSG:3031']
SUPPORTED_REQUESTS = [
    "GetMap",
    "GetCapabilities",
    "GetMetadata",
    "GetColorbar",
    "GetLegendGraphic",
]
SUPPORTED_FORMATS = ["image/png", "image/jpeg"]
SUPPORTED_EXCEPTIONS = ["XML"]

EXCEPTION_TEMPLATE = """<?xml version='1.0' encoding="UTF-8"?>
<ServiceExceptionReport version="1.3.0"
    xmlns="http://www.opengis.net/ogc"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:py="http://genshi.edgewall.org/"
    xsi:schemaLocation="http://www.opengis.net/ogc http://schemas.opengis.net/wms/1.3.0/exceptions_1_3_0.xsd">
    <ServiceException {error_code}>
        {message}
    </ServiceException>
</ServiceExceptionReport>"""

ALLOWED_EXCEPTIONS = [
    "InvalidFormat",
    "InvalidCRS",
    "LayerNotDefined",
    "StyleNotDefined",
    "MissingDimensionValue",
    "InvalidDimensionValue",
    "OperationNotSupported",
]


class WMSException(Exception):
    def __init__(self, message, status_code=400, error_code=""):
        # Call the base class constructor with the parameters it needs
        if error_code:
            error_code = f'code="{error_code}"'
        msg = EXCEPTION_TEMPLATE.format(message=message, error_code=error_code)
        super().__init__(msg)
        self.status_code = status_code
        self.error_code = error_code


@dataclass
class MVTRequest:
    """
    Class which holds (common) query params for a MVT request
    """

    time: datetime.datetime | str = None
    x: int | str = None
    y: int | str = None
    z: int | str = None
    # 0 means points, 1 means lines, 2 means polygons
    # 0 currently also means points with angle info (vectors)
    # 1 means contours
    # 2 means contour polygons
    dim: int | str = None
    # Variables separeted by : as in layers for WMS
    variables: T.Sequence[str] | str = None
    # Cmap needed for contours (which to draw) - could also be a levels param
    cmap: str = None

    @classmethod
    def from_query_string(cls, query: T.Mapping[str, T.Any]) -> "MVTRequest":
        """Parses query string into WMSRequest"""
        parsed = dict()
        for f in fields(cls):
            k = f.name
            if k in query:
                parsed[k] = query[k]
            elif (upper := k.upper()) in query:
                parsed[k] = query[upper]
        return cls(**parsed)

    def validate(self) -> None:
        """
        Validate and convert fields from strings
        May raise ValueError
        """
        required_args = ("time", "x", "y", "z", "dim", "variables")
        for arg in required_args:
            if getattr(self, arg) is None:
                raise ValueError(f"{arg} not present in request")

        if not isinstance(self.time, datetime.datetime):
            if self.time.lower() == "current":
                self.time = datetime.datetime.now(tz=datetime.timezone.utc).strftime(
                    "%Y-%m-%dT%H:%M:%S.%fZ"
                )
            try:
                try:
                    self.time = datetime.datetime.strptime(
                        self.time, "%Y-%m-%dT%H:%M:%S.%fZ"
                    ).replace(tzinfo=datetime.timezone.utc)
                except ValueError:
                    self.time = datetime.datetime.strptime(
                        self.time, "%Y-%m-%dT%H:%M:%SZ"
                    ).replace(tzinfo=datetime.timezone.utc)
            except ValueError:
                raise
        for k in ("x", "y", "z", "dim"):
            val = getattr(self, k)
            if not isinstance(val, int):
                # May raise value error
                val = int(val)
                setattr(self, k, val)
        if isinstance(self.variables, str):
            self.variables = self.variables.split(":")
        # Now test combinations of dim and vars
        if self.dim not in (0, 1, 2):
            raise ValueError("dim must be in 0, 1, 2")
        if (self.dim == 0 and len(self.variables) != 2) or (
            self.dim != 0 and len(self.variables) != 1
        ):
            raise ValueError("dim=0 requires 2 variables, dim=1,2 requires exactly 1")
        if self.dim > 0 and self.cmap is None:
            raise ValueError("cmap needed for contours")


@dataclass
class WMSRequest:
    """
    Class which holds raw request strings for a WMS request
    After calling validate values can be parsed to e.g. a list of strings etc
    """

    request: str = None
    bbox: T.Sequence[float] | str = None
    cmap: str = None
    layers: T.Sequence[str] | str = None
    width: int | str = None
    height: int | str = None
    transparent: bool | str = True
    # Dimensions
    time: datetime.datetime | str = "current"
    level: int | str = None
    elevation: float | str = None
    styles: T.Sequence[str] | str = None
    service: str = "WMS"
    version: str = WMS_VERSION
    format: str = "image/png"
    crs: str = None
    exceptions: str = "XML"
    size: int | str = 400
    items: str = None
    # Not used below here, just ignored
    bgcolor: str = None
    expr: str | T.Mapping[str, T.Sequence[float]] = None
    bounds: str = None
    legend_options: str = None
    format_options: str = None

    @classmethod
    def from_query_string(cls, query: T.Mapping[str, T.Any]) -> "WMSRequest":
        """Parses query string into WMSRequest"""
        parsed = dict()
        for f in fields(cls):
            k = f.name
            if k in query:
                parsed[k] = query[k]
            elif (upper := k.upper()) in query:
                parsed[k] = query[upper]
        return cls(**parsed)

    def validate(self) -> None:
        """
        Validate (format fields etc) - raises WMSException
        """
        if self.request not in SUPPORTED_REQUESTS:
            msg = msg = f"REQUEST not valid; valid values={SUPPORTED_REQUESTS}"
            raise WMSException(msg, 400)
        if self.request == "GetMap":
            self.validate_get_map()
        elif self.request in ("GetColorbar", "GetLegendGraphics"):
            self.validate_get_colorbar()
        elif self.request == "GetMetadata":
            self.validate_get_metadata()

    def validate_get_metadata(self) -> None:
        """
        Validate GetMetadata
        """
        if isinstance(self.layers, str):
            self.layers = [lyr.strip() for lyr in self.layers.split(",")]
        if isinstance(self.items, str):
            self.items = [item.strip() for item in self.items.split(",")]

    def validate_get_colorbar(self) -> None:
        """
        Validate GetColorbar call.
        """
        if self.cmap is None:
            raise WMSException("CMAP must be specified", 400)

        try:
            self.size = int(self.size)
        except ValueError:
            raise WMSException(f"SIZE={self.size} not an int", 400)

        if self.size > MAX_WIDTH or self.size < MIN_WIDTH:
            msg = f"SIZE={self.size} must be in range [{MIN_WIDTH}; {MAX_WIDTH}]"
            raise WMSException(msg, 400)

        if isinstance(self.layers, str):
            self.layers = [lyr.strip() for lyr in self.layers.split(",")]

    def validate_get_map(self) -> None:
        # Validate all GetMap params
        required_args = (
            "version",
            "request",
            "layers",
            "styles",
            "crs",
            "bbox",
            "width",
            "height",
            "format",
        )
        for arg in required_args:
            if getattr(self, arg) is None:
                msg = "%s not present in request" % arg.upper()
                raise WMSException(msg, 400)

        # Check that VERSION is VERSION=1.3.0
        if self.version != WMS_VERSION:
            msg = f"VERSION={self.version} not supported; valid value={WMS_VERSION}"
            raise WMSException(msg, 400)

        if self.format not in SUPPORTED_FORMATS:
            msg = (
                f"FORMAT={self.format} not supported; valid values={SUPPORTED_FORMATS}"
            )
            error_code = "InvalidFormat"
            raise WMSException(msg, 400, error_code)
        try:
            self.width = int(self.width)
        except ValueError:
            msg = f"WIDTH={self.width} not an integer"
            raise WMSException(msg, 400)

        if self.width < MIN_WIDTH or self.width > MAX_WIDTH:
            msg = f"WIDTH={self.width} must be in range [{MIN_WIDTH}; {MAX_WIDTH}]"
            raise WMSException(msg, 400)

        try:
            self.height = int(self.height)
        except ValueError:
            msg = f"HEIGHT={self.height} not an integer"
            raise WMSException(msg, 400)

        if self.height < MIN_HEIGHT or self.width > MAX_HEIGHT:
            msg = f"HEIGHT={self.height} must be in range [{MIN_HEIGHT}; {MAX_HEIGHT}]"
            raise WMSException(msg, 400)

        if self.crs not in SUPPORTED_CRS:
            msg = f"CRS={self.crs} not supported; valid values={SUPPORTED_CRS}"
            error_code = "InvalidCRS"
            raise WMSException(msg, 400, error_code)
        if isinstance(self.bbox, str):
            bbox = self.bbox.split(",")

        if len(bbox) != 4:
            msg = f"BBOX={self.bbox} must contain 4 comma separated values"
            raise WMSException(msg, 400)

        try:
            bbox = [float(v) for v in bbox]
        except ValueError:
            msg = f"BBOX={self.bbox} does not contain 4 numeric values"
            raise WMSException(msg, 400)

        # Reorder bounding box for EPSG:4326 which has lat/lon ordering
        if self.crs == "EPSG:4326":
            bbox = [bbox[1], bbox[0], bbox[3], bbox[2]]
        # All ok with bbox
        self.bbox = bbox

        if isinstance(self.layers, str):
            layers = [lyr.strip() for lyr in self.layers.split(",")]

        if isinstance(self.styles, str):
            if not self.styles:
                # Empty string - use default
                styles = ["default"] * len(layers)
            else:
                styles = [style.strip() for style in self.styles.split(",")]

        if len(layers) != len(styles):
            msg = f"LAYERS={self.layers} and STYLES={self.styles} do not have same length ({len(layers)} != {len(styles)})"
            raise WMSException(msg, 400)

        self.layers = layers
        self.styles = styles

        if not isinstance(self.transparent, bool):
            if self.transparent not in ["TRUE", "FALSE", "true", "false"]:
                msg = f"TRANSPARENT={self.transparent} must be in TRUE, FALSE, true, false"
                raise WMSException(msg, 400)
            # Convert to bool
            self.transparent = True if self.transparent.lower() == "true" else False

        if self.exceptions not in SUPPORTED_EXCEPTIONS:
            msg = f"EXCEPTIONS={self.exceptions} not supported; valid values={SUPPORTED_EXCEPTIONS}"
            raise WMSException(msg, 400)

        if not isinstance(self.time, datetime.datetime):
            if self.time.lower() == "current":
                self.time = datetime.datetime.now(tz=datetime.timezone.utc).strftime(
                    "%Y-%m-%dT%H:%M:%S.%fZ"
                )
            try:
                try:
                    self.time = datetime.datetime.strptime(
                        self.time, "%Y-%m-%dT%H:%M:%S.%fZ"
                    ).replace(tzinfo=datetime.timezone.utc)
                except ValueError:
                    self.time = datetime.datetime.strptime(
                        self.time, "%Y-%m-%dT%H:%M:%SZ"
                    ).replace(tzinfo=datetime.timezone.utc)
            except ValueError:
                raise WMSException(f"TIME={self.time} is invalid", 400)

        # Check ELEVATION (TODO: add check)
        if self.elevation is not None:
            try:
                self.elevation = float(self.elevation)
            except ValueError:
                msg = f"ELEVATION={self.elevation} not a float"
                raise WMSException(msg, 400)

        # Check level (TODO: add check)
        if self.level is not None:
            try:
                self.level = int(self.level)
            except ValueError:
                msg = f"LEVEL={self.level} not an int"
                raise WMSException(msg, 400)
        if isinstance(self.expr, str):
            try:
                self.expr = json.loads(self.expr)
            except Exception as e:
                raise ValueError(f"Could not parse expr: {str(e)}")
