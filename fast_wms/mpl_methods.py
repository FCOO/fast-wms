import typing as T
from functools import lru_cache
import random
import time

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.pyplot import setp
from matplotlib.ticker import FormatStrFormatter
from matplotlib.patheffects import withStroke
from matplotlib.axes import Axes
import numpy as np
from osgeo import osr

# from labellines import labelLines

from .styles import Style
from .arrow_barbs import arrow_barbs
from .vector import contours_as_lines


# Seed
random.seed("hestepappa")


# NOTE: ONLY process safe
@lru_cache(maxsize=128, typed=False)
def get_fig_and_ax(shape: T.Tuple[int, int]):
    fig = Figure(figsize=(shape[1] / 80, shape[0] / 80), dpi=80)
    ax = fig.add_axes([0.0, 0.0, 1.0, 1.0])
    fig.patch.set_alpha(0.0)
    ax.patch.set_alpha(0.0)
    return fig, ax


# Borrowed from pydap
default_format_options = {
    "antialias": None,
    "vectorMinLength": 0.5,
    "vectorAntiAliasing": True,
    "contourLabelAntiAliasing": True,
    "contourLabelStroke": True,
    "contourLabelStrokeColor": "white",
    "contourLabelStrokeWidth": 2,
    "contourLabelSize": 11,
    "contourLine": False,
    "contourLineLabels": True,
    "contourGridLabels": False,
    "contourLineWidth": 1.5,
    "contourLineColor": "black",
    "contourLineStrokeColor": "white",
    "contourLineStroke": True,
    "contourLineStrokeWidth": 3,
    "mapAntiAliasing": False,
    "contourLineAntiAliasing": True,
}

barb_incs = {"half": 2.57222, "full": 5.14444, "flag": 25.7222}


def draw_vector_field(
    style_def: Style,
    dx: np.ndarray,
    nd1: float,
    dy: np.ndarray,
    nd2: float,
    bbox: T.Sequence[float],
    format_options: T.Dict[str, T.Any] = default_format_options,
) -> np.ndarray | None:
    """
    Draw vector field U, V at pos X, Y on axes.
    Return image as rgba data in form of np.ndarray of shape (h, w, 4)
    """
    t1 = time.perf_counter()
    fig, ax = get_fig_and_ax(dx.shape)
    t2 = time.perf_counter()
    # print("Get ax: ", t2 - t1)
    vector_spacing = style_def.vector_spacing or 40
    nrows = int(dx.shape[0] / vector_spacing)
    ncols = int(dy.shape[1] / vector_spacing)
    csx = (bbox[2] - bbox[0]) / ncols
    csy = (bbox[3] - bbox[1]) / nrows
    X, Y = np.meshgrid(
        np.linspace(bbox[0] + csx * 0.5, bbox[2] - csx * 0.5, ncols),
        np.linspace(bbox[1] + csy * 0.5, bbox[3] - csy * 0.5, nrows),
    )
    J = ((X - bbox[0]) / (bbox[2] - bbox[0]) * dx.shape[1]).astype(np.int32)
    I = ((bbox[3] - Y) / (bbox[3] - bbox[1]) * dx.shape[0]).astype(np.int32)
    U = dx[I, J]
    V = dy[I, J]
    mask = np.logical_and(U != nd1, V != nd2)
    if not mask.any():
        return None
    I, J = np.where(mask)
    if I.size:
        X = X[I, J]
        Y = Y[I, J]
        U = U[I, J]
        V = V[I, J]
    C = np.sqrt(U**2 + V**2)
    # Construct min norm (everything less than this is set to zero and drawn as a dot)
    if style_def.colormap is None:
        # Works for 'black vectors'
        vmin = 0
        norm = None
        cmap = None
        s = np.where(C <= vmin, 0, 1 / C)
    else:
        idx_min = 1
        if style_def.colormap.extend in ("both", "min"):
            idx_min = 0
        vmin = style_def.colormap.levels[idx_min] * format_options["vectorMinLength"]
        norm = style_def.colormap.mp_norm
        cmap = style_def.colormap.mp_cmap
        # Trouble if vmin is <= 0
        s = np.where(C < vmin, 0, 1 / C)
    ax.clear()
    ax.axis((bbox[0], bbox[2], bbox[1], bbox[3]))
    ax.axis("off")
    vector_method = style_def.plot_method
    vector_color = "k"
    if vector_method == "black_barbs":
        sizes = {
            "spacing": 0.2,
            "width": 0.2,
            "headwidth": 1.0,
            "headlength": 1.0,
            "headaxislength": 1.0,
            "emptybarb": 0.2,
        }
        arrow_barbs(
            ax,
            X,
            Y,
            U,
            V,
            pivot="middle",
            length=5.5,
            linewidth=1,
            color=vector_color,
            edgecolor="w",
            antialiased=format_options["vectorAntiAliasing"],
            fill_empty=True,
            sizes=sizes,
            barb_increments=barb_incs,
        )
    elif vector_method == "black_arrowbarbs":
        sizes = {
            "spacing": 0.2,
            "width": 0.2,
            "headwidth": 2.0,
            "headlength": 1.0,
            "headaxislength": 1.25,
            "emptybarb": 0.2,
        }
        arrow_barbs(
            ax,
            X,
            Y,
            U,
            V,
            pivot="middle",
            length=5.5,
            facecolor=vector_color,
            edgecolor="w",
            antialiased=format_options["vectorAntiAliasing"],
            fill_empty=True,
            sizes=sizes,
            barb_increments=barb_incs,
        )
    elif vector_method == "color_quiver1":
        ax.quiver(
            X,
            Y,
            U * s,
            V * s,
            C,
            pivot="middle",
            units="inches",
            scale=3.0,
            scale_units="inches",
            width=0.11,
            linewidths=1.0,
            headwidth=2,
            headlength=2,
            headaxislength=2,
            edgecolors=("k"),
            antialiased=True,
            norm=norm,
            cmap=cmap,
        )
    elif vector_method == "color_quiver2":
        ax.quiver(
            X,
            Y,
            U * s,
            V * s,
            C,
            pivot="middle",
            units="inches",
            scale=4.0,
            scale_units="inches",
            width=0.0825,
            linewidths=1.0,
            headwidth=2,
            headlength=1,
            headaxislength=1,
            edgecolors=("k"),
            antialiased=True,
            norm=norm,
            cmap=cmap,
        )
    elif vector_method == "color_quiver3":
        ax.quiver(
            X,
            Y,
            U * s,
            V * s,
            C,
            pivot="middle",
            units="inches",
            scale=4.0,
            scale_units="inches",
            width=0.05,
            linewidths=1.0,
            headwidth=3,
            headlength=1.5,
            headaxislength=1.5,
            edgecolors=("k"),
            antialiased=True,
            norm=norm,
            cmap=cmap,
        )

    else:
        # if vector_method == 'black_quiver':
        ax.quiver(
            X,
            Y,
            U * s,
            V * s,
            pivot="middle",
            units="inches",
            scale=4.0,
            scale_units="inches",
            width=0.04,
            color=vector_color,
            linewidths=1,
            headlength=4,
            headaxislength=3.5,
            minlength=2,
            edgecolors=("w"),
            antialiased=True,
        )
    canvas = FigureCanvas(fig)
    canvas.draw()  # Draw the canvas, cache the renderer
    return np.asarray(canvas.buffer_rgba())


def mslext2text(ncchars: str) -> T.Tuple[str, str]:
    """\
    Convert text of H and L's (high and low pressures) to TeXlike plot
    format
    """
    colors = {"H": "blue", "L": "red"}
    # ncstring = str(netCDF4.chartostring(ncchars)).strip()
    ncstring = ncchars
    if ":" in ncstring:
        sym, val = ncstring.split(":")
        hlt = sym + "\n" + val
        return hlt, colors[sym]
    return ncstring, "black"


def plot_annotations(
    data: T.Sequence[str],
    coords: T.Sequence[T.Tuple[float, float]],
    ax: Axes,
    bbox: T.Sequence[float],
    shrink: float = 0.1,
):
    """Plot string variable annotations."""
    dx = (bbox[2] - bbox[0]) * shrink
    dy = (bbox[3] - bbox[1]) * shrink
    # Loop over extrema
    for xy, te in zip(coords, data):
        # Extract string and color
        if (
            bbox[0] + dx <= xy[0] <= bbox[2] - dx
            and bbox[1] + dy <= xy[1] <= bbox[3] - dy
        ):
            pltstr, color = mslext2text(te)
            ax.text(
                xy[0],
                xy[1],
                pltstr,
                color=color,
                ha="center",
                va="center",
                fontsize=24,
                path_effects=[withStroke(linewidth=2.0, foreground="w")],
                zorder=100,
            )


def draw_contour_levels(
    style_def: Style,
    # Possibly a masked array
    values: np.ndarray,
    bbox: T.Sequence[float],
    format_options: T.Dict[str, T.Any] = default_format_options,
) -> np.ndarray | None:
    """
    Draw contours
    """
    t1 = time.perf_counter()
    fig, ax = get_fig_and_ax(values.shape)
    t2 = time.perf_counter()
    # print("Get ax: ", t2 - t1)
    levels = style_def.colormap.levels[:]
    if style_def.colormap.extend in ["min", "neither"]:
        levels = levels[:-1]
    if style_def.colormap.extend in ["max", "neither"]:
        levels = levels[1:]
    ax.clear()
    ax.axis((bbox[0], bbox[2], bbox[1], bbox[3]))
    ax.axis("off")
    halfcsx = (bbox[2] - bbox[0]) / values.shape[1] * 0.5
    halfcsy = (bbox[3] - bbox[1]) / values.shape[0] * 0.5
    cs = ax.contour(
        np.linspace(bbox[0] + halfcsx, bbox[2] - halfcsx, values.shape[1]),
        # Backwards
        np.linspace(bbox[3] - halfcsy, bbox[1] + halfcsy, values.shape[0]),
        values,
        levels=levels,
        colors=format_options["contourLineColor"],
        linewidths=format_options["contourLineWidth"],
        antialiased=format_options["contourLineAntiAliasing"],
    )
    if format_options["contourLineLabels"]:
        fltfmt = FormatStrFormatter("%d")
        cs.levels = [fltfmt(val) for val in cs.levels]
        clbls = ax.clabel(
            cs,
            inline=1,
            fontsize=format_options["contourLabelSize"],
            use_clabeltext=True,
        )
        setp(
            clbls,
            path_effects=[
                withStroke(
                    linewidth=format_options["contourLabelStrokeWidth"],
                    foreground=format_options["contourLabelStrokeColor"],
                )
            ],
        )
    if format_options["contourLineStroke"]:
        setp(
            cs.collections,
            path_effects=[
                withStroke(
                    linewidth=format_options["contourLineStrokeWidth"],
                    foreground=format_options["contourLabelStrokeColor"],
                )
            ],
        )
    canvas = FigureCanvas(fig)
    canvas.draw()  # Draw the canvas, cache the renderer
    return np.asarray(canvas.buffer_rgba())


def draw_contour_levels2(
    style_def: Style,
    # Possibly a masked array
    values: np.ndarray,
    nd_val: float,
    data_mask: np.ndarray,
    bbox: T.Sequence[float],
    # Bounding box of values array
    bbox_values: T.Sequence[float],
    transform: osr.CoordinateTransformation,
    buf_pct: float,
    width_out: int,
    height_out: int,
    annotations: T.Tuple[T.Sequence[T.Tuple[float, float]], T.Sequence[str]] = None,
    format_options: T.Dict[str, T.Any] = default_format_options,
    # Flip xy for transformation
    flip_xy: bool = False,
) -> np.ndarray | None:
    """
    Draw contours
    """
    # t1 = time.perf_counter()
    fig, ax = get_fig_and_ax((height_out, width_out))
    # t2 = time.perf_counter()
    # print("Get ax: ", t2 - t1)
    levels = style_def.colormap.levels[:]
    if style_def.colormap.extend in ["min", "neither"]:
        levels = levels[:-1]
    if style_def.colormap.extend in ["max", "neither"]:
        levels = levels[1:]
    # Like masked min max
    rv = values[data_mask]
    rmin = rv.min()
    rmax = rv.max()
    if levels[0] < rmin:
        for n, lvl in enumerate(levels):
            if lvl >= rmin:
                break
        levels = levels[n:]
    if levels[-1] >= rmax:
        for n, lvl in enumerate(levels[::-1]):
            if lvl <= rmax:
                break
        levels = levels[:-n]
    if not levels.shape[0]:
        return None
    t1 = time.perf_counter()
    lines = contours_as_lines(
        values, nd_val, bbox_values, levels, gauss_sigma=style_def.gaussian_filter
    )
    t2 = time.perf_counter()
    # print(t2 - t1, " contours", values.shape, bbox_values, transform)
    ax.clear()
    ax.axis((bbox[0], bbox[2], bbox[1], bbox[3]))
    ax.axis("off")
    xrad = bbox[2] - bbox[0]
    yrad = bbox[3] - bbox[1]
    x_labels = int(width_out / 100)
    y_labels = int(height_out / 100)
    rad = max(xrad, yrad)
    labels = []
    label_pos = []
    rotations = []
    shrink = 0.1 + buf_pct
    # Allowd area for labels
    x2 = bbox[2] - shrink * xrad
    x1 = bbox[0] + shrink * xrad
    y2 = bbox[3] - shrink * yrad
    y1 = bbox[1] + shrink * yrad
    xrad_shrink = x2 - x1
    yrad_shrink = y2 - y1
    # A matrix for claiming positions
    label_grid = np.zeros((y_labels, x_labels), dtype=bool)
    # total_dt = 0
    for z, line in lines:
        if transform is not None:
            if flip_xy:
                line = line[:, ::-1]
            line = np.asarray(transform.TransformPoints(line))[:, :2]
        # print(dt)
        ll = np.sqrt(((line[1:] - line[:-1]) ** 2).sum(axis=1)).sum()
        if ll > 0.3 * rad:
            M = (line < (x2, y2)).all(axis=1)
            M &= (line > (x1, y1)).all(axis=1)
            I = np.where(M)[0]
            if I.shape[0] > 5:
                line_cut = line[I]
                i = random.randint(1, line_cut.shape[0] - 2)
                p1 = line_cut[i]
                # Check pos
                py = int((y2 - p1[1]) / yrad_shrink * y_labels)
                px = int((p1[0] - x1) / xrad_shrink * x_labels)
                if not label_grid[py, px]:
                    label_grid[py, px] = True
                    p0 = line_cut[i - 1]
                    p2 = line_cut[i + 1]
                    dv = p2 - p0
                    rotation = np.rad2deg(np.arctan2(dv[1], dv[0]))
                    if rotation > 90:
                        rotation -= 180
                    elif rotation < -90:
                        rotation += 180
                    labels.append("%.0f" % z)
                    rotations.append(rotation)
                    label_pos.append(p1)
        ax.plot(
            line[:, 0],
            line[:, 1],
            color="k",
            linewidth=format_options["contourLineWidth"],
            antialiased=True,
        )
    # labelLines(lines2label, zorder=2.5, shrink_factor=0.1)
    for rot, pos, label in zip(rotations, label_pos, labels):
        ax.text(
            pos[0],
            pos[1],
            label,
            rotation=rot,
            backgroundcolor=(0.97, 0.97, 0.97, 0.9),
            color="k",
            fontsize="small",
            ha="center",
            va="center",
            antialiased=True,
        )

    if annotations:
        plot_annotations(
            data=annotations[1],
            coords=annotations[0],
            ax=ax,
            bbox=bbox,
            shrink=buf_pct + 0.02,
        )
    canvas = FigureCanvas(fig)
    canvas.draw()  # Draw the canvas, cache the renderer
    return np.asarray(canvas.buffer_rgba())
