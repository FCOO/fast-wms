import datetime
import typing as T
import json
import numpy as np


def ncv_wrapper(
    ncvfile: str,
    time_inp: datetime.datetime,
    # TODO: Add this as env var
    replace: T.Tuple[str, str] = ("/server/data/data", "/data"),
    return_additonal_metadata: bool = False,
    # Should we allow times < min_time - 0.5 * dt and > max_time + 0.5 dt ?
    extend_time: bool = False,
) -> str | tuple[str, list[datetime.datetime], list[float]]:
    """
    Open NCV file and find closest time match
    Also return times and bbox if requested
    TODO: Clean up time handling here by using cftime
    """
    with open(ncvfile) as f:
        ncv = json.load(f)
    # First find closest timestep
    epoch = datetime.datetime.strptime(
        ncv["time"]["units"], "seconds since %Y-%m-%d %H:%M:%S"
    ).replace(tzinfo=datetime.timezone.utc)
    times = np.array(ncv["time"]["value"])
    #
    # Default is to return last filename / newest file
    #
    tidx = times.shape[0] - 1
    #
    dt = times[1] - times[0]
    # Otherwise use time_inp
    if time_inp is not None:
        # Test if times use constant spacing (regular grid)
        dt_epoch = (time_inp - epoch).total_seconds()
        diffs = times[1:] - times[:-1]
        if (diffs == diffs[0]).all():
            tidx = int(round(dt_epoch / dt))
        else:
            tidx = np.argmin(np.fabs(times - dt_epoch))
            if tidx == 0 and dt_epoch < -dt * 0.5:
                tidx = -1
            if tidx == times.shape[0] - 1 and dt_epoch > times[-1] + dt * 0.5:
                tidx = times.shape[0]

    if extend_time:
        tidx = min(max(tidx, 0), len(times) - 1)
    # Test if outta bounds
    if tidx < 0 or tidx >= times.shape[0]:
        tval = None
    else:
        tval = times[tidx]
    if return_additonal_metadata:
        times = [epoch + datetime.timedelta(seconds=dt) for dt in ncv["time"]["value"]]
        grid = ncv["grid"]
        # In EPSG:4326, Always I suppose
        bbox = [grid["xs"], grid["ys"], grid["xe"], grid["ye"]]
    datasets = list(ncv["dataset"].keys())
    ds = ncv["dataset"][datasets[0]]
    selected_fname: str = None
    if tval is not None:
        min_dist = None
        # These time grids might not be regular either, so handle this by finding closest one
        for tvals, fname in zip(ds["time"], ds["files"]):
            tvals = [val for val in tvals if val != ""]
            if tval < tvals[0] - dt * 0.5 or tval > tvals[-1] + dt * 0.5:
                continue
            diffs = np.fabs(tvals - tval)
            idx = np.argmin(diffs)
            dist = diffs[idx]
            if dist == 0:
                selected_fname = fname
                min_dist = 0
                break
            if min_dist is None or dist < min_dist:
                selected_fname = fname
                min_dist = dist
        assert selected_fname is not None
        # Perhaps assert something about min_dist as well
        if replace:
            selected_fname = selected_fname.replace(replace[0], replace[1])
    if return_additonal_metadata:
        return selected_fname, times, bbox
    return selected_fname
