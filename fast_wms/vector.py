# import os
# from pathlib import Path
# import time
import typing as T

import numpy as np
import mapbox_vector_tile
from osgeo import ogr, gdal, osr
from scipy.ndimage import gaussian_filter


def vectors_as_mvt(
    dx: np.ndarray,
    dy: np.ndarray,
    lyrname: str,
    mask: np.ndarray,
    n_rows: int = 16,
    n_cols: int = 16,
):
    """
    Return a point grid containing a vector field (dx, dy) as MVT
    """
    features = []
    rowsize = dx.shape[0] // n_rows
    colsize = dx.shape[1] // n_cols
    space_y = rowsize // 2
    space_x = colsize // 2
    scale_x = 4096 // dx.shape[0]
    scale_y = 4096 // dy.shape[0]
    # print(dx.dtype, dx.min(), dx.max(), "HHJHJH")
    for i in range(n_rows):
        rowpos = i * rowsize + space_y
        for j in range(n_cols):
            colpos = j * colsize + space_x
            vdx = float(dx[rowpos, colpos])
            vdy = float(dy[rowpos, colpos])
            # Funky, can compare float and np.float32 at array level, but not as scalars
            if mask[rowpos, colpos]:
                features.append(
                    {
                        "geometry": "POINT({0:d} {1:d})".format(
                            colpos * scale_x, 4096 - rowpos * scale_y
                        ),
                        "properties": {
                            "dx": float(vdx),
                            "dy": float(vdy),
                        },
                    }
                )
            # print(int(dx[rowpos, colpos] * 100), "HHDHHDHD")
    return mapbox_vector_tile.encode({"name": lyrname, "features": features})


def dataset_to_counturs(
    ds: gdal.Dataset,
    cstr: str,
    layername: str,
    ogr_drv: ogr.Driver,
    levels: T.Iterable[float],
    fieldnames: T.Iterable[str],
    outdim: int = 1,
) -> tuple[ogr.DataSource, ogr.Layer]:
    """
    Create contours as ogr datasource from gdal ds
    """
    ogr_ds = ogr_drv.CreateDataSource(cstr)
    srs_wkt = ds.GetProjection()
    srs = osr.SpatialReference(srs_wkt) if srs_wkt else None
    geomtype = ogr.wkbMultiPolygon if outdim == 2 else ogr.wkbLineString
    lyr = ogr_ds.CreateLayer(layername, srs, geomtype)
    band: gdal.Band = ds.GetRasterBand(1)
    if outdim == 1:
        field_defn = ogr.FieldDefn("ID", ogr.OFTInteger)
        lyr.CreateField(field_defn)
        field_defn = ogr.FieldDefn(fieldnames[0], ogr.OFTReal)
        lyr.CreateField(field_defn)
        gdal.ContourGenerate(
            ds.GetRasterBand(1), 0, 0, levels, 1, band.GetNoDataValue(), lyr, 0, 1
        )
    else:
        field_defn = ogr.FieldDefn("ID", ogr.OFTInteger)
        lyr.CreateField(field_defn)
        field_defn = ogr.FieldDefn(fieldnames[0], ogr.OFTReal)
        lyr.CreateField(field_defn)
        field_defn = ogr.FieldDefn(fieldnames[1], ogr.OFTReal)
        lyr.CreateField(field_defn)
        gdal.ContourGenerateEx(
            band,
            lyr,
            options=[
                "FIXED_LEVELS=" + ",".join([str(lvl) for lvl in levels]),
                "ID_FIELD=0",
                "ELEV_FIELD_MIN=1",
                "ELEV_FIELD_MAX=2",
                "POLYGONIZE=TRUE",
            ],
        )

    # Ok - so now polygonize that - use the mask as ehem... mask...

    return ogr_ds, lyr


OGR_MEMORY_DRIVER = ogr.GetDriverByName("Memory")
GDAL_MEMORY_DRIVER = gdal.GetDriverByName("Mem")


def contours_as_lines(
    rast: np.ndarray,
    nd_val: float,
    bbox: T.Sequence[float],
    levels: T.Iterable[float],
    gauss_sigma: int = 7,
) -> list[tuple[float, np.ndarray]]:
    """
    Generate contours and return coordinates as numpy arrays
    """
    datatype = gdal.GDT_Float64 if rast.dtype == np.float64 else gdal.GDT_Float32
    ds1 = GDAL_MEMORY_DRIVER.Create("ds1", rast.shape[1], rast.shape[0], 1, datatype)
    csx = (bbox[2] - bbox[0]) / rast.shape[1]
    csy = (bbox[3] - bbox[1]) / rast.shape[0]
    ds1.SetGeoTransform((bbox[0], csx, 0, bbox[3], 0, -csy))
    band = ds1.GetRasterBand(1)
    band.SetNoDataValue(float(nd_val))
    band.WriteArray(rast)
    nd_mask = rast == nd_val
    if (nd_mask).any() and gauss_sigma > 0:
        # t1 = time.perf_counter()
        gdal.FillNodata(
            band,
            maskBand=None,
            maxSearchDist=4 * gauss_sigma + 1,
            smoothingIterations=0,
        )
        # t2 = time.perf_counter()
        # print("Fill: ", t2 - t1)
    if gauss_sigma > 0:
        rast2 = gaussian_filter(band.ReadAsArray(), gauss_sigma)
        rast2[nd_mask] = nd_val
        band.WriteArray(rast2)

    # print(nd_val, (rast == nd_val).sum(), rast.max(), rast.min())
    # rr = rast[rast != nd_val]
    # print(rr.max(), rr.min(), "real")
    fieldnames = ["h"]
    ogr_ds, lyr = dataset_to_counturs(
        ds1, "dummy", "elevation", OGR_MEMORY_DRIVER, levels, fieldnames, 1
    )
    # So far so good, now encode
    features = []
    for feat in lyr:
        line = np.asarray(feat.GetGeometryRef().GetPoints())
        # x1, y1 = line.min(axis=0)
        # x2, y2 = line.max(axis=0)
        z = feat.GetField(1)
        features.append((z, line))
    return features


def contours_as_mvt(
    rast: np.ndarray,
    nd_val: float,
    outdim: int,
    lyrname: str,
    levels: T.Iterable[float],
):
    """
    Get contours (lines or polygons) as MVT
    """
    datatype = gdal.GDT_Float64 if rast.dtype == np.float64 else gdal.GDT_Float32
    ds1 = GDAL_MEMORY_DRIVER.Create("ds1", rast.shape[1], rast.shape[0], 1, datatype)
    cs = 4096 / rast.shape[0]
    ds1.SetGeoTransform((0, cs, 0, 4096, 0, -cs))
    band = ds1.GetRasterBand(1)
    band.SetNoDataValue(float(nd_val))
    band.WriteArray(rast)
    fieldnames = ["h"] if outdim == 1 else ["h1", "h2"]
    ogr_ds, lyr = dataset_to_counturs(
        ds1, "dummy", "elevation", OGR_MEMORY_DRIVER, levels, fieldnames, outdim
    )
    # So far so good, now encode
    features = []
    for feat in lyr:
        geom = bytes(feat.GetGeometryRef().ExportToWkb())
        props = (
            {"h1": feat.GetField(1), "h2": feat.GetField(2)}
            if outdim == 2
            else {"h": feat.GetField(1)}
        )
        features.append({"geometry": geom, "properties": props})
    # lyr = None
    # ogr_ds = None
    # ds1 = None
    return mapbox_vector_tile.encode({"name": lyrname, "features": features})


# TODO: Cache this hard
# Dunno if we really need to encode lyrname

EMPTY_MVT_CACHE = {}


def get_empty_mvt(lyrname: str):
    """
    Just an empty vector tile
    """
    res = EMPTY_MVT_CACHE.get(lyrname)
    if res is not None:
        return res
    res = mapbox_vector_tile.encode({"name": lyrname, "features": []})
    EMPTY_MVT_CACHE[lyrname] = res
    return res
