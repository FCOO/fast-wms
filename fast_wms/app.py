"""
Fast api app.
We use a process pool to do the real work in order to avoid problems with thread safety (netcdf)
"""

import datetime
import asyncio
from concurrent.futures import ProcessPoolExecutor
import multiprocessing
import os
import io
import typing as T

import uvicorn as uvicorn
from fastapi import FastAPI, HTTPException, APIRouter, Request, Response
from fastapi.responses import JSONResponse

# from fastapi.concurrency import run_in_threadpool

# from starlette.responses import StreamingResponse, JSONResponse
from fastapi.responses import Response
from fastapi.middleware.cors import CORSMiddleware

from .image import render_image, get_mvt, get_colorbar_img

# from .utils import get_closest_time
from . import capabilities
from .validation import WMSException, WMSRequest, MVTRequest
from . import logging


logger = logging.get_logger("app")

# Initialize FastAPI app
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
)

# Route with a prefix
router = APIRouter()

# Number of processes in pool
pool_size = int(os.getenv("POOL_SIZE", 2 * multiprocessing.cpu_count() + 1))


executor = ProcessPoolExecutor(max_workers=pool_size)


async def mvt_handler(full_path: str, req: Request):
    """
    Handle MVT request
    """
    mvt_request = MVTRequest.from_query_string(req.query_params)
    try:
        mvt_request.validate()
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    loop = asyncio.get_event_loop()
    # NOTE:
    # We can use executor._pending_work_items to check how many tasks are pending and possibly return a 503 or something
    # if things get out of hand!
    try:
        out = await loop.run_in_executor(executor, get_mvt, full_path, mvt_request)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    return Response(out, media_type="binary/octet-stream")


async def get_capabilities(ncfile: str, location: str) -> str:
    """
    Get WMS Capabilities
    """
    loop = asyncio.get_event_loop()
    try:
        xml = await loop.run_in_executor(
            executor, capabilities.get_capability_xml, ncfile, location
        )
    except ValueError as e:
        # raise
        raise HTTPException(status_code=400, detail=str(e))
    return xml


async def get_metadata_json(
    ncfile: str, layers: T.Sequence[str], items: T.Sequence[str]
) -> T.Dict[str, T.Any]:
    """
    Get custom metadata as JSON
    """
    loop = asyncio.get_event_loop()
    try:
        data = await loop.run_in_executor(
            executor, capabilities.get_metadata, ncfile, layers, items
        )
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    return data


async def get_colorbar(
    width: int, height: int, cmap: str, ncfile: str = None, ncvar: str = None
) -> io.BytesIO:
    loop = asyncio.get_event_loop()
    try:
        out = await loop.run_in_executor(
            executor, get_colorbar_img, width, height, cmap, ncfile, ncvar
        )
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    return out


# WMS endpoint
# Should support both upper and lower case query params
async def wms_handler(ncfile: str, req: Request):
    """
    WMS handler
    """
    wms_request = WMSRequest.from_query_string(req.query_params)
    try:
        wms_request.validate()
    except WMSException as e:
        return Response(
            content=str(e), media_type="application/xml", status_code=int(e.status_code)
        )
    if wms_request.request == "GetCapabilities":
        url = str(req.url)
        # proto = url.split("://")[0]
        host = url.split("://")[1].split("/")[0]
        location = str(req.url).split("?")[0]
        if x_host := req.headers.get("x-forwarded-host"):
            location = location.replace(host, x_host)
        xml = await get_capabilities(ncfile, location)
        return Response(content=xml, media_type="application/xml")
    if wms_request.request == "GetMetadata":
        data = await get_metadata_json(
            ncfile, wms_request.layers, items=wms_request.items
        )
        return JSONResponse(content=data)
    if wms_request.request in ("GetColorbar", "GetLegendGraphics"):
        # We can make a special request to include layer info, if layers are also specified
        ncvar = None
        if wms_request.layers:
            h = max(32, int(wms_request.size * 0.22))
            # Only first, its an error to send more layers
            ncvar = wms_request.layers[0].split(":")[0]
        else:
            h = max(32, int(wms_request.size * 0.15))
        out = await get_colorbar(
            wms_request.size, h, wms_request.cmap, ncfile=ncfile, ncvar=ncvar
        )
        return Response(out.getvalue(), media_type="image/png")

    assert wms_request.request == "GetMap"
    # Now things have been translated to the format the render_image needs
    # We could possible send more WMS standard variables to render_image and transform them there instead

    loop = asyncio.get_event_loop()
    # NOTE:
    # We can use executor._pending_work_items to check how many tasks are pending and possibly return a 503 or something
    # if things get out of hand!
    try:
        out = await loop.run_in_executor(
            executor,
            render_image,
            ncfile,
            wms_request,
        )
    except Exception as e:
        logger.exception(e)
        e = WMSException(repr(e))
        # TODO: Log traceback
        return Response(content=str(e), media_type="application/xml", status_code=400)
    return Response(out.getvalue(), media_type=wms_request.format)


@router.get("/{full_path:path}")
async def root(full_path: str, req: Request):
    """
    Generic endpoint which can distribute to wms or mvt, etc.
    """
    full_path = "/" + full_path
    # Support .wms suffix as with pydap
    if full_path.endswith(".wms"):
        full_path = full_path[:-4]
        return await wms_handler(full_path, req)
    if full_path.endswith(".mvt"):
        full_path = full_path[:-4]
        return await mvt_handler(full_path, req)
    # Return something defaulty
    raise HTTPException(status_code=400, detail=f"No handler found for {full_path}")


app.include_router(router, prefix=os.getenv("PREFIX", "/webmap/v3"))
app.include_router(router, prefix="/webmap/v2")

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
