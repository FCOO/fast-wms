import typing as T
import datetime
import io
from dataclasses import dataclass

# from pathlib import Path
# import json
import logging
import time
import math

# from functools import lru_cache
# import time
# import pdb

from PIL import Image, ImageDraw, ImageFont
import numpy as np
from osgeo import gdal, osr
import netCDF4 as nc


from . import vector
from .vnetcdf import ncv_wrapper
from .utils import get_ncmetadata, get_ndval
from . import utils
from .capabilities import get_default_style
from . import mpl_methods
from .styles import COLORS, parse_style, Style
from .validation import WMSRequest, MVTRequest
from . import srs_handling
from .srs_handling import Rect
from .logging import get_logger

logger = get_logger("image")


def _get_empty_img(width: int, height: int, format="png") -> io.BytesIO:
    img = Image.new("P", (width, height))
    img.putpalette([0, 0, 0, 0], rawmode="RGBA")
    out = io.BytesIO()
    img.save(out, format)
    out.seek(0)
    return out


EMPTY_512_PNG = _get_empty_img(512, 512, "png").getvalue()


def get_empty_img(width: int, height: int, format="png") -> io.BytesIO:
    if "width" == 512 and "height" == 512 and format == "png":
        return io.BytesIO(EMPTY_512_PNG)
    return _get_empty_img(width, height, format)


def empty_pil_img(width: int, height: int) -> Image.Image:
    """
    Return fully transparent image as PIL Image
    """
    img = Image.new("P", (width, height))
    img.putpalette([0, 0, 0, 0], rawmode="RGBA")
    return img


def get_sig_fig(num):
    num = str(float(num))
    if num.endswith(".0"):
        return 0
    else:
        return len(num.split(".")[-1]) if "." in num else 0


def get_colorbar_img(
    width: int,
    height: int,
    cmap: str,
    ncfile: str = None,
    ncvar: str = None,
) -> io.BytesIO:
    """
    Get colorbar
    """
    if ncfile and ncvar:
        # Get the unit and standard_name
        if ncfile.endswith(".ncv"):
            ncfile = ncv_wrapper(
                ncfile, datetime.datetime.now(datetime.timezone.utc), extend_time=True
            )
        with nc.Dataset(ncfile) as ds:
            var = ds.variables[ncvar]
            sn = getattr(var, "long_name", getattr(var, "standard_name", ""))
            if len(sn) > 40:
                sn = getattr(var, "standard_name", "")
            unit = getattr(var, "units", "NA")
        toptext = f"{sn} [{unit}]"
        top = int(0.25 * height)
        vsize = int(height * 0.5)
    else:
        top = 0
        toptext = None
        vsize = int(height * 0.6)
    img = Image.new(mode="P", size=(width, height))
    colors = COLORS[cmap]
    fontsize = max(min(int(width * 0.044), int(height * 0.24)), 8)
    left = max(fontsize, int(width) * 0.02)
    right = left
    w = (width - left - right) / len(colors.base_colors)

    out = io.BytesIO()
    img = Image.new("P", (width, height), color=colors.transparent_val)
    img.putpalette(colors.pallette, rawmode="RGBA")
    draw = ImageDraw.Draw(img)
    img.putpalette(colors.pallette, rawmode="RGBA")

    if (height - top - vsize - 2) > fontsize:
        font = ImageFont.truetype(
            "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", fontsize
        )
    else:
        font = None
    if toptext and font:
        draw.text((right, 2), toptext, fill=colors.black_val, font=font, anchor="lt")
    idx_level = 0
    prev_label_max = None
    for i in range(len(colors.base_colors)):
        if i == 0 and colors.extend in ("min", "both"):
            draw.polygon(
                [
                    (left, top + vsize * 0.5),
                    (left + w, top),
                    (left + w, top + vsize),
                    (left, top + vsize * 0.5),
                ],
                fill=0,
                outline=colors.black_val,
            )
            continue
        elif i == len(colors.base_colors) - 1 and colors.extend in ("both", "max"):
            x = left + (i + 1) * w
            draw.polygon(
                [
                    (x, top + vsize * 0.5),
                    (x - w, top),
                    (x - w, top + vsize),
                    (x, top + vsize * 0.5),
                ],
                fill=i,
                outline=colors.black_val,
            )
        else:
            draw.rectangle(
                [i * w + left, top, (i + 1) * w + left, top + vsize],
                fill=i,
                outline=colors.black_val,
            )
        if font is not None:
            ndigs = get_sig_fig(colors.levels[idx_level])
            txt = f"%.{ndigs}f" % colors.levels[idx_level]
            next_len = font.getlength(txt)
            pos_x = i * w + left
            if prev_label_max is None or pos_x - next_len * 0.5 > prev_label_max + 2:
                draw.text(
                    (pos_x, top + vsize + 2),
                    txt,
                    fill=colors.black_val,
                    font=font,
                    anchor="ma",
                )
                prev_label_max = pos_x + next_len * 0.5

            idx_level += 1
    img.save(out, format="png")
    out.seek(0)
    return out


@dataclass
class MDSliceMeta:
    """
    Holds information about the slices we need from a multidimensional variable
    """

    # 2D grid shape
    xsize: int
    ysize: int
    # 2D grid slice
    x0: int
    x1: int
    y0: int
    y1: int
    # time
    tidx: int
    # z
    level: int


def ncvar_as_gdal(
    ncfile: str,
    # Spatial reference of request
    srs: osr.SpatialReference,
    # Bbox in srs of request
    bbox: T.Sequence[float],
    # Epsg code of request (superfluous)
    epsg_code: str,
    time_inp: datetime.datetime,
    ncvars: T.Iterable[str],
    level: int = None,
    elevation: float = None,
    # Whether or not to use min time for time_inp < min_time and max_time for time_inp > max_time
    # Otherwise round down to nearest valid time
    extend_time: bool = False,
    # How many cells to buffer
    buffer: int = 1,
    # Complement to buffer, use at least this amount of points in each dimension
    min_size: int = 1,
    # min dim should be at least
    annotations_out: T.Dict[str, T.Any] = None,
) -> T.List[T.Union[gdal.Dataset, None]]:
    rasters: list[gdal.Dataset] = []
    # Store dims - cache common calculations
    dims_cache: T.Dict[str, tuple[list, MDSliceMeta]] = {}

    with nc.Dataset(ncfile) as ds:
        # If we use nearest neighbor for regridding / reprojection - buffer is not really important
        # A tiny optimization would be to decide already here what regridding algo we need
        src_srs, src_wkt, idx_lon, idx_lat, densify = srs_handling.identify_srs(ds)
        if src_srs == srs_handling.dmi_nea or src_srs == srs_handling.dmi_igb:
            buffer = max(3, buffer)
        if src_srs == srs:
            src_bbox = bbox[:]
            transform = None
        else:
            # Handle input bbox
            transform = osr.CoordinateTransformation(srs, src_srs)
            if densify:
                # Instead we could buffer bbox
                pts = transform.TransformBounds(bbox[0], bbox[1], bbox[2], bbox[3], 8)
                src_bbox = [pts[idx_lon], pts[idx_lat], pts[idx_lon + 2], pts[idx_lat + 2]]
            else:
                # This is faster
                pts = transform.TransformPoints(
                    [
                        (bbox[0], bbox[1]),
                        (bbox[0], bbox[3]),
                        (bbox[2], bbox[1]),
                        (bbox[2], bbox[3]),
                    ]
                )
                lats = sorted([pt[idx_lat] for pt in pts])
                lons = sorted([pt[idx_lon] for pt in pts])
                # We sometimes need to buffer for a weird rotated coord sys
                # In order to really get a proper bbox we would need to transform more points along lines
                src_bbox = [lons[0], lats[0], lons[-1], lats[-1]]
        # print(src_bbox)
        # See if pole is contained
        if epsg_code == "3413" and src_wkt == srs_handling.wkt_4326:
            if bbox[0] <= 0 <= bbox[2] and bbox[1] <= 0 <= bbox[3]:
                src_bbox[3] = 90
            if bbox[0] < 0 and bbox[3] / bbox[0] == -1:
                src_bbox[0] = -180.0
                src_bbox[2] = 180
            elif bbox[0] < 0 and bbox[1] / bbox[0] == -1:
                src_bbox[2] = 179.9
                src_bbox[0] = lons[1]
        for ncvar in ncvars:
            var = ds.variables[ncvar]
            key = var.dimensions
            if key in dims_cache:
                res = dims_cache[key]
            else:
                res = get_geotransform_and_indexes(
                    ds,
                    var,
                    src_bbox=src_bbox,
                    time_inp=time_inp,
                    buffer=buffer,
                    min_size=min_size,
                    elevation=elevation,
                    extend_time=extend_time,
                    src_srs_is_geographic=src_srs.IsGeographic(),
                )
                dims_cache[key] = res
            # print(res)
            if res is None:
                rasters.append(None)
                continue
            geotransform, sm = res
            if elevation is not None and sm.level is not None:
                # Use level calculated from elevation if this is available
                level = sm.level
            # This may or may not set set_auto_mask(False)
            nd_val = get_ndval(var)
            if var.ndim == 4:
                if level is None:
                    # Default to first level
                    level = 0
                level = min(max(0, level), var.shape[1] - 1)
                rast = var[
                    sm.tidx,
                    level,
                    (sm.ysize - sm.y1 - 1) : (sm.ysize - sm.y0),
                    sm.x0 : (sm.x1 + 1),
                ]
            else:
                rast = var[
                    sm.tidx,
                    (sm.ysize - sm.y1 - 1) : (sm.ysize - sm.y0),
                    sm.x0 : (sm.x1 + 1),
                ]

            if np.ma.is_masked(rast) and rast.fill_value != nd_val:
                nd_val = rast.fill_value
            rast = np.ma.getdata(rast, False)
            rast = rast[::-1, :]
            datatype = (
                gdal.GDT_Float64 if rast.dtype == np.float64 else gdal.GDT_Float32
            )
            # print(rast.max(), rast.min(), rast.dtype)
            ds1 = gdal.GetDriverByName("MEM").Create(
                "ds1", rast.shape[1], rast.shape[0], 1, datatype
            )

            ds1.SetGeoTransform(
                (
                    geotransform[0] + sm.x0 * geotransform[1],
                    geotransform[1],
                    0,
                    geotransform[3] + sm.y0 * geotransform[5],
                    0,
                    geotransform[5],
                )
            )
            ds1.SetProjection(src_wkt)
            band = ds1.GetRasterBand(1)
            band.SetNoDataValue(float(nd_val))
            band.WriteArray(rast)
            rasters.append(ds1)
            if (
                annotations_out is not None
                and (avarname := getattr(var, "annotations", None)) is not None
            ):
                t1 = time.perf_counter()
                if avarname in ds.variables:
                    avar = ds.variables[avarname]
                    arr = np.ma.getdata(avar[sm.tidx, :, :])
                    coords = avar.coordinates.split()
                    # Always in epsg4326??
                    inv_transform = osr.CoordinateTransformation(
                        srs_handling.epsg4326, srs
                    )
                    # Lat, Lon
                    lat = np.ma.getdata(ds.variables[coords[0]][sm.tidx, :])
                    lon = np.ma.getdata(ds.variables[coords[1]][sm.tidx, :])
                    mask = np.ma.getmask(ds.variables[coords[0]][sm.tidx, :])
                    pts = []
                    text = []
                    for idx in range(arr.shape[0]):
                        if mask[idx]:
                            break
                        pts.append((lat[idx], lon[idx]))
                        text.append(("".join(x.decode() for x in arr[idx, :])).strip())
                    xy = inv_transform.TransformPoints(pts)
                    annotations_out[ncvar] = (xy, text)
                t2 = time.perf_counter()
                # print("ANNO: ", t2 - t1)
                # extract_annotations(ds, avar, annotations_out, transform=transform)

        return rasters, src_srs, transform


def get_geotransform_and_indexes(
    ds: nc.Dataset,
    var: nc.Variable,
    src_bbox: T.Sequence[float],
    time_inp: datetime.datetime,
    buffer: int,
    min_size: int,
    elevation: float,
    extend_time: bool = False,
    src_srs_is_geographic: bool = True,
) -> tuple[list, float, MDSliceMeta] | None:
    """
    Calculate GDAL georeference of variable and calculate the slices we need in 2d coords
    as well as time and level indexes
    """
    extents = get_ncmetadata(ds, var.dimensions)
    # Assuming (time, lat, lon) or (time, z, lat, lon)
    # Geotransform of whole dataset (not subwindow)

    geotransform = [
        extents[-1][0] - extents[-1][2] * 0.5,
        extents[-1][2],
        0,
        extents[-2][1] + extents[-2][2] * 0.5,
        0,
        -extents[-2][2],
    ]
    # Check if lons are > 180
    if src_srs_is_geographic and extents[-1][1] > 180:
        geotransform[0] = geotransform[0] - 360

    xsize = extents[-1][3]
    ysize = extents[-2][3]
    # Raster coords
    x0 = int((src_bbox[0] - geotransform[0]) / geotransform[1])
    y0 = int((src_bbox[3] - geotransform[3]) / geotransform[5])
    x1 = int((src_bbox[2] - geotransform[0]) / geotransform[1])
    y1 = int((src_bbox[1] - geotransform[3]) / geotransform[5])
    # Perhaps < 0
    disjoint1 = x1 <= 0 or x0 >= xsize
    disjoint2 = y1 <= 0 or y0 >= ysize
    if disjoint1 or disjoint2:
        return None
    x0 = max(x0, 0)
    y0 = max(y0, 0)
    x1 = min(x1, xsize - 1)
    y1 = min(y1, ysize - 1)
    if x1 < x0 or y1 < y0:
        return None
    # Check if we should buffer to avoid artifacts at tile boundaries
    if buffer or min_size > 1:
        buffer = max(buffer, (min_size - (x1 - x0)) // 2, (min_size - (y1 - y0)) // 2)

        x0 = max(x0 - buffer, 0)
        y0 = max(y0 - buffer, 0)
        x1 = min(x1 + buffer, xsize - 1)
        y1 = min(y1 + buffer, ysize - 1)
        # print(buffer, "buffer", min_size, x1 - x0, y1 - y0)

    tvar = ds[var.dimensions[0]]
    # This is slower but seems to handle non regular time spacing as well
    calendar = getattr(tvar, "calendar", "standard")
    # Check if outta time range?
    num = nc.date2num(time_inp, tvar.units, calendar=calendar)
    half_time_step = extents[0][2] * 0.5
    # print(num, half_time_step, extents[0])
    if (
        num < (extents[0][0] - half_time_step)
        or (num > extents[0][1] + half_time_step)
        and not extend_time
    ):
        return None
    tidx = nc.date2index(time_inp, tvar, select="nearest", calendar=calendar)
    # Check elevation for ndim == 4
    level = None
    if elevation is not None and var.ndim == 4:
        # This overrides level
        zvals = np.ma.getdata(ds.variables[var.dimensions[1]][:])
        # Calculate level
        M = np.isclose(zvals, elevation)
        if not M.any():
            logger.info("No z level close to %s", elevation)
            return None
        level = np.where(M)[0][0]
    return geotransform, MDSliceMeta(xsize, ysize, x0, x1, y0, y1, tidx, level)


def pixels2meters(px: int, py: int, zoom, tile_size: int = 256) -> T.Tuple[float]:
    "Converts pixel coordinates in given zoom level of pyramid to EPSG:900913"
    initial_resolution = 2 * math.pi * 6378137 / tile_size
    # 156543.03392804062 for tileSize 256 pixels
    origin_shift = 2 * math.pi * 6378137 / 2.0
    res = initial_resolution / (2**zoom)
    mx = px * res - origin_shift
    my = origin_shift - py * res
    return mx, my, res


def tile_bounds(tx: int, ty: int, zoom: int, tile_size: int = 256):
    "Returns bounds of the given tile in EPSG:900913 coordinates"

    minx, maxy, res = pixels2meters(tx * tile_size, ty * tile_size, zoom)
    maxx, miny, res = pixels2meters((tx + 1) * tile_size, (ty + 1) * tile_size, zoom)
    return (minx, miny, maxx, maxy), res


def num2deg(xtile, ytile, zoom):
    n = 1 << zoom
    lon_deg = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    lat_deg = math.degrees(lat_rad)
    return lat_deg, lon_deg


def get_mvt(
    ncfile: str,
    mvt_request: MVTRequest,
) -> bytes:
    """
    Get MVT containing points with dx, dy attrs
    to be styled as vector fields, or contour lines or contour polygons
    """
    if mvt_request.dim == 0:
        # Vectors
        lyrname = "dir"
    else:
        # Contours
        lyrname = "contours"
    # TODO: Apply smoothing and tile expansions as for WMS contours
    if mvt_request.dim == 0 and len(mvt_request.variables) != 2:
        raise ValueError("Need two variables for vector field")
    if ncfile.endswith(".ncv"):
        ncfile = ncv_wrapper(ncfile, mvt_request.time)
        if ncfile is None:
            return vector.get_empty_mvt(lyrname)
    bbox, _ = tile_bounds(mvt_request.x, mvt_request.y, mvt_request.z, 256)
    srs = srs_handling.epsg3857
    datasources, _, _ = ncvar_as_gdal(
        ncfile,
        srs,
        bbox,
        "3857",
        time_inp=mvt_request.time,
        ncvars=mvt_request.variables,
        # For now, always use top level if 3D field
        level=0,
    )
    rasters = []
    alg = gdal.GRA_Bilinear if mvt_request.dim == 0 else gdal.GRA_CubicSpline
    for ds1 in datasources:
        if ds1 is not None:
            rout = reproject_raster(ds1, 256, 256, bbox, srs=srs, reproject_algo=alg)
            rasters.append(rout)
    if mvt_request.dim == 0 and len(rasters) == 2:
        mask = np.logical_and(
            rasters[0][0] != rasters[0][1], rasters[1][0] != rasters[1][1]
        )
        if mask.any():
            data = vector.vectors_as_mvt(
                rasters[0][0], rasters[1][0], lyrname, mask, 8, 8
            )
        else:
            data = vector.get_empty_mvt(lyrname)
    elif mvt_request.dim > 0 and rasters:
        cmap = COLORS.get(mvt_request.cmap)
        if cmap is None:
            raise ValueError("No such cmap")
        # Merge rasters
        r1, nd1 = rasters[0]
        for rast2 in rasters[1:]:
            r2, nd2 = rast2
            mask = r2 != nd2
            r1[mask] = r2[mask]
        mask = r1 != nd1
        if mask.any():
            data = vector.contours_as_mvt(
                r1, nd1, mvt_request.dim, "elevation", levels=cmap.levels
            )
        else:
            data = vector.get_empty_mvt(lyrname)
    else:
        data = vector.get_empty_mvt(lyrname)
    return data


def reproject_raster(
    ds1: T.Any,
    width: int,
    height: int,
    bbox: T.Iterable[float],
    srs: T.Any,
    reproject_algo: int = gdal.GRA_Bilinear,
):
    # t1 = time.time()
    memdriver = gdal.GetDriverByName("MEM")
    band = ds1.GetRasterBand(1)
    datatype = band.DataType
    nd_val = band.GetNoDataValue()
    ds2 = memdriver.Create("ds2", width, height, 1, datatype)
    csx = (bbox[2] - bbox[0]) / width
    csy = (bbox[1] - bbox[3]) / height
    ds2.SetGeoTransform(
        (
            bbox[0],
            csx,
            0,
            bbox[3],
            0,
            csy,
        )
    )
    wkt_out = srs.ExportToWkt()
    ds2.SetProjection(wkt_out)
    band = ds2.GetRasterBand(1)
    band.SetNoDataValue(nd_val)
    band.Fill(nd_val)
    res = gdal.ReprojectImage(
        ds1,
        ds2,
        None,  # wkt_4326,
        None,  # wkt_out,
        reproject_algo,
        maxerror=0.2,
    )
    ds1 = None
    rast = band.ReadAsArray()
    ds2 = None
    t2 = time.time()
    # print("Reproj", t2 - t1)
    # print("algo: ", reproject_algo, rast.min(), rast.max())
    return rast, nd_val


def render_image(
    ncfile: str,
    wms_request: WMSRequest,
    verbose: bool = False,
) -> io.BytesIO:
    """
    Very simple implementation of GetMap request with time and possibly level
    TODO: Add elevation as a supplement to level
    """
    # Special syntax for layers composed of several variables
    # Could be done in validation I suppose
    varnames = [lyr.split(":") for lyr in wms_request.layers]
    # Translate HTTP format code to PIL name
    format = wms_request.format.split("/")[-1]
    # Get EPSG code
    epsg_code = wms_request.crs.removeprefix("EPSG:")
    srs = srs_handling.SRS[epsg_code]
    # Handle vnetcdf (TODO: Add while, could be recursive)
    if ncfile.endswith(".ncv"):
        ncfile = ncv_wrapper(ncfile, wms_request.time)
        if ncfile is None:
            return get_empty_img(wms_request.width, wms_request.height)
    #
    # Parse styles
    #
    style_defs: T.Sequence[Style] = []
    for style, ncvars in zip(wms_request.styles, varnames):
        if style == "default":
            style = get_default_style(ncfile, ncvars)
            logger.debug("Defualt style: %s", style)
            if style is None:
                raise ValueError(f"No default style for {ncvars}")
        style_def = parse_style(style, cmap_str=wms_request.cmap)
        style_defs.append(style_def)
    #
    # These values might change during calculation due to buffering etc
    # original values are kept in wms_request
    #
    width = wms_request.width
    height = wms_request.height
    bbox = wms_request.bbox[:]  # A copy
    csx = (wms_request.bbox[2] - wms_request.bbox[0]) / width
    # Note - this is negative (GDAL convention)
    csy = (wms_request.bbox[1] - wms_request.bbox[3]) / height
    # Flatten varnames to a single list
    ncvars_flat: T.List[str] = []
    for group in varnames:
        ncvars_flat.extend(group)
    #
    # Hacky hack: Dict to hold (mslext) text annotations
    annotations: T.Dict[str, T.Any] = None
    # Object to hold pixel buffers in each direction
    tile_buf_pixels: Rect = None
    #
    # Buffer to add to tile (rad) when drawing contours
    # TODO: consider adding buffer to image always, just smaller
    # TODO: Buffer according to src_resolution
    buf_pct = 0.01
    min_size = 1
    for st in style_defs:
        if st.plot_method != "contour":
            continue
        # Less boundary trouble for contours?
        # The correct way to know how much to buffer our bbox is really to transform bbox and see
        # how many source points we have inside bbox. And then iterate.
        # For now settle for a poor mans heuristic
        min_size = 10 * style_def.gaussian_filter
        annotations = {}
        proj_bounds = srs_handling.SRS_BBOX.get(epsg_code)
        # In this case we also need some extra buffer around image
        buf_pct = 0.10
        # Heuristic
        zoom = round(math.log2((proj_bounds.x2 - proj_bounds.x1) / csx))
        if zoom > 12:
            buf_pct += (zoom - 12) * 0.02
        dpx = int(width * buf_pct)
        dpy = int(height * buf_pct)
        dx = csx * dpx
        # Negative
        dy = csy * dpy
        # right, top, left, bottom
        tile_buf_pixels = Rect(0, 0, 0, 0)
        if not proj_bounds or proj_bounds.x1 < bbox[0] - dx:
            bbox[0] -= dx
            tile_buf_pixels.x1 = dpx
        if not proj_bounds or proj_bounds.x2 > bbox[2] + dx:
            bbox[2] += dx
            tile_buf_pixels.x2 = dpx
        if not proj_bounds or proj_bounds.y1 < bbox[1] + dy:
            bbox[1] += dy
            # Bottom
            tile_buf_pixels.y2 = dpy
        if not proj_bounds or proj_bounds.y2 > bbox[3] - dy:
            bbox[3] -= dy
            # Bottom
            tile_buf_pixels.y1 = dpy
        width += tile_buf_pixels.x1 + tile_buf_pixels.x2
        height += tile_buf_pixels.y1 + tile_buf_pixels.y2
        break
    #
    # Construct list of GDAL datasets for each needed ncvar (from flat list)
    #
    datasets, src_srs, transform = ncvar_as_gdal(
        ncfile,
        srs,
        bbox,
        epsg_code,
        min_size=min_size,
        time_inp=wms_request.time,
        ncvars=ncvars_flat,
        level=wms_request.level,
        elevation=wms_request.elevation,
        annotations_out=annotations,
    )
    #
    # Check source resolution - smoothing and rendering method may be determined on basis of this information
    #
    src_resolution = None
    src_resolution_max = None
    for ds in datasets:
        if ds is None:
            continue
        band = ds.GetRasterBand(1)
        src_resolution = min(band.XSize / width, band.YSize / height)
        src_resolution_max = max(band.XSize / width, band.YSize / height)
    if src_resolution is None:
        return get_empty_img(wms_request.width, wms_request.height)
    #
    # Group GDAL datasets in same way as varnames
    #
    grouped_datasets = []
    idx_dataset = 0
    for group in varnames:
        grouped_datasets.append(datasets[idx_dataset : idx_dataset + len(group)])
        idx_dataset += len(group)
    # Output for reprojected rasters (np.ndarray
    rasters: T.List[T.List[np.ndarray]] = []
    # Raster styles (some styles for layers that are drawn directly or out of bounds may be skipped)
    raster_styles: T.List[Style] = []
    # Keep the varnames that are not skipped for some reason here
    raster_varnames: T.List[T.List[str]] = []
    # Final list of images and masks (one for each layer)
    imgs: T.List[T.Tuple[Image.Image, Image.Image]] = []
    for group, style_def, ds_group in zip(varnames, style_defs, grouped_datasets):
        rast_group = []
        for ds in ds_group:
            # TODO: This should be handled better.
            # If e.g. one of the rasters in a group could not be generated (outta bounds)
            # we should probably skip the whole group in most cases
            if ds is None:
                continue
            if style_def.plot_method == "contour" and src_resolution_max < 1:
                # For contours it does not make a lotta sense to reproject to much higher resolution than src data
                # Heuristic - in this case it is faster to draw contours on source array and reproject afterwatds
                # In this case we do NOT append a raster to be handled later.
                # Instead we add an image to imgs directly
                anno_tuple = annotations.get(group[0])
                img, mask = draw_contours_directly(
                    ds,
                    transform=transform,
                    src_srs=src_srs,
                    anno_tuple=anno_tuple,
                    style_def=style_def,
                    bbox=bbox,
                    buf_pct=buf_pct,
                    width=width,
                    height=height,
                )
                imgs.append((img, mask))
            else:
                reproj_width = width
                reproj_height = height
                # print(src_resolution, src_resolution_max, "RES")
                if (
                    style_def.is_vector
                    or epsg_code == "3413"
                    or style_def.plot_method == "contour"
                ):
                    reproject_algo = gdal.GRA_NearestNeighbour
                elif src_resolution > 0.5:
                    reproject_algo = gdal.GRA_Bilinear
                else:
                    reproject_algo = gdal.GRA_CubicSpline
                rast, nd_val = reproject_raster(
                    ds,
                    reproj_width,
                    reproj_height,
                    bbox,
                    srs=srs,
                    reproject_algo=reproject_algo,
                )
                # print("AFTER PROJ", rast.max(), rast.min(), rast.dtype)
                rast_group.append((rast, nd_val))
        if rast_group:
            rasters.append(rast_group)
            raster_styles.append(style_def)
            raster_varnames.append(group)

    #
    # Merge rasters before drawing if possible
    # This can be done with two (or more) layers of same dimension and same style
    #
    # NOTE: This could actually be done as a reduce or windowing operation for greater flexibility
    # For now merge all or nothing starting from first raster
    # TODO: More intelligent merging (not all or nothing)
    dimensions = [len(group) for group in rasters]
    style_dim = len(set(st.style_str for st in raster_styles))
    if len(rasters) > 1 and style_dim == 1 and len(set(dimensions)) == 1:
        # Merge each dimension
        # [[(r1, nd1)], [(r2, nd2)]]
        rg1 = rasters[0]
        for rg2 in rasters[1:]:
            for dim in range(dimensions[0]):
                r1, nd1 = rg1[dim]
                r2, nd2 = rg2[dim]
                mask = r2 != nd2
                r1[mask] = r2[mask]
        rasters = [rg1]
        raster_styles = [raster_styles[0]]
        # TODO: If the merged rasters have annotations we should really merge these as well
        raster_varnames = [raster_varnames[0]]

    for ncvars, rg, style_def in zip(raster_varnames, rasters, raster_styles):
        anno_tuple = None
        if style_def.plot_method == "contour":
            # xy, text (lists)
            anno_tuple = annotations.get(ncvars[0])
        elif wms_request.expr is not None:
            raster_out, ndval_out = generate_impacts(
                rg,
                varnames=ncvars,
                impact_def=wms_request.expr,
                levels=style_def.colormap.levels,
            )
            rg = [(raster_out, ndval_out)]
        img = layer_to_img(
            style_def,
            rg,
            bbox=bbox,
            buf_pct=buf_pct,
            width_out=width,
            height_out=height,
            annotations=anno_tuple,
            verbose=verbose,
        )
        imgs.append(img)
    if not imgs:
        return get_empty_img(wms_request.width, wms_request.height)
    img, mask = imgs[0]
    # In case of more than one layer (group of rasters)
    if len(imgs) > 1:
        # Hmm - ndmask of second image may cut vectors
        # Isn't it better to merge grids before rendering vectors?
        # We do that above if all styles and all "dimensions" (len of groups) are the same
        for img2, mask in imgs[1:]:
            # Might f*** up if palletes are different :)
            # TODO: If so, convert to RGBA
            img.paste(img2, mask=mask)
    if tile_buf_pixels:
        img = img.crop(
            [
                tile_buf_pixels.x1,
                tile_buf_pixels.y1,
                width - tile_buf_pixels.x2,
                height - tile_buf_pixels.y2,
            ]
        )
        width, height = img.size
        assert width == wms_request.width
    #
    # Switch for adding antialiasing - NOT used ATM
    #
    if wms_request.width < width:
        # Antialias
        # t1 = time.perf_counter()
        img = (
            img.convert("RGBA")
            .resize((wms_request.width, wms_request.height), Image.Resampling.BICUBIC)
            .quantize()
        )
        # t2 = time.perf_counter()
        # print(t2 - t1)
    out = io.BytesIO()
    # t1 = time.perf_counter()
    img.save(out, format=format)
    # t2 = time.perf_counter()
    # print("Saving png ", t2 - t1)
    out.seek(0)
    return out


def generate_impacts(
    rg: T.Sequence[T.Tuple[np.ndarray, float]],
    varnames: T.Sequence[str],
    impact_def: T.Mapping[str, T.Sequence[float]],
    levels: np.ndarray,
) -> T.Tuple[np.ndarray, float]:
    #
    # Handle impact_limits
    #
    # Logic:
    #  * Defualt is nodata
    #  * Something is 'good' (lowest color) if < or > first limit for all vars
    #  * Something is level (color n + 1) if >= or <= limit n for any var
    #  * Highest level counts, etc.

    deltas = levels[1:] - levels[:-1]
    ndval_out = levels[0] - 0.5 * deltas[0]
    raster_out = np.ones_like(rg[0][0]) * ndval_out

    for rast, varname in zip(rg, varnames):
        r1, ndval = rast
        mask = r1 != ndval
        limits = impact_def.get(varname)
        if len(limits) > 1:
            sign = np.sign(limits[1] - limits[0])
        else:
            sign = 1
        if len(limits) > len(levels) - 2:
            raise ValueError(
                "Limit sequence for impacts must have at most len(colormap.levels) - 2 elements"
            )
        # Set green (or lowest color)
        imp_mask = np.logical_and(raster_out < levels[0], mask)
        if sign > 0:
            imp_mask &= r1 < limits[0]
        else:
            imp_mask &= r1 > limits[0]
        raster_out[imp_mask] = levels[0] + 0.5 * deltas[0]
        # Set color 1,..
        for i in range(len(limits)):
            # We can never go lower than we already are
            imp_mask = np.logical_and(raster_out < levels[i + 1], mask)
            if sign > 0:
                imp_mask &= r1 >= limits[i]
            else:
                imp_mask &= r1 <= limits[i]
            val = levels[i + 1] + 0.5 * deltas[i + 1]
            raster_out[imp_mask] = val
    return raster_out, ndval_out


def draw_contours_directly(
    ds: gdal.Dataset,
    transform: osr.CoordinateTransformation,
    src_srs: osr.SpatialReference,
    anno_tuple,
    style_def: Style,
    bbox: T.Sequence[float],
    buf_pct: float,
    width: int,
    height: int,
) -> T.Tuple[Image.Image, Image.Image]:
    """
    Draw contours directly in source projection
    """
    band = ds.GetRasterBand(1)
    rast = band.ReadAsArray()
    nd_val = band.GetNoDataValue()
    georef = ds.GetGeoTransform()
    bbox_rast = [
        georef[0],
        georef[3] + ds.RasterYSize * georef[5],
        georef[0] + ds.RasterXSize * georef[1],
        georef[3],
    ]
    flip_xy = False
    if src_srs == srs_handling.epsg4326:
        flip_xy = True
        bbox_rast[0] = max(bbox_rast[0], -180)
        bbox_rast[2] = min(bbox_rast[2], 180)
    inv_transform = None
    if transform is not None:
        inv_transform = transform.GetInverse()
    data_mask = rast != nd_val
    # t1 = time.perf_counter()
    rgba = mpl_methods.draw_contour_levels2(
        style_def,
        rast,
        nd_val,
        data_mask,
        bbox,
        bbox_rast,
        transform=inv_transform,
        buf_pct=buf_pct,
        width_out=width,
        height_out=height,
        annotations=anno_tuple,
        flip_xy=flip_xy,
    )
    # t2 = time.perf_counter()
    # print(t2 - t1, "draw")
    if rgba is None:
        img, mask = empty_pil_img(width, height), Image.fromarray(
            np.zeros((height, width), dtype=bool)
        )
    else:
        img, mask = Image.fromarray(rgba).quantize(64), Image.fromarray(data_mask)
    return img, mask


def layer_to_img(
    style_def: Style,
    # 1 or 2 of (arr, nd_val)
    rasters: T.Sequence[T.Tuple[np.ndarray, float]],
    bbox: T.Sequence[float],
    buf_pct: float,
    width_out: int,
    height_out: int,
    annotations: T.Tuple[T.Sequence[T.Tuple[float, float]], T.Sequence[str]] = None,
    verbose: bool = False,
) -> T.Tuple[Image.Image, Image.Image]:
    if style_def.is_vector:
        # t1 = time.perf_counter()
        assert len(rasters) == 2
        dx, nd1 = rasters[0]
        dy, nd2 = rasters[1]
        nd_mask = np.logical_or(dx == nd1, dy == nd2)
        rgba = mpl_methods.draw_vector_field(style_def, dx, nd1, dy, nd2, bbox)
        if rgba is None:
            return empty_pil_img(*dx.shape[::-1]), Image.fromarray(~nd_mask)
        # t2 = time.perf_counter()
        # print("MPL ", t2 - t1)
        return Image.fromarray(rgba).quantize(64), Image.fromarray(~nd_mask)
    elif style_def.plot_method == "contour" or style_def.plot_method == "contour2":
        # t1 = time.perf_counter()
        assert len(rasters) == 1
        rast, nd_val = rasters[0]
        data_mask = rast != nd_val
        if not data_mask.any():
            return empty_pil_img(*rast.shape[::-1]), Image.fromarray(data_mask)
        # t1 = time.perf_counter()
        if style_def.plot_method == "contour2":
            rgba = mpl_methods.draw_contour_levels(
                style_def, np.ma.masked_array(data=rast, mask=~data_mask), bbox=bbox
            )
        else:
            rgba = mpl_methods.draw_contour_levels2(
                style_def,
                rast,
                nd_val,
                data_mask,
                bbox=bbox,
                bbox_values=bbox,
                transform=None,
                buf_pct=buf_pct,
                width_out=width_out,
                height_out=height_out,
                annotations=annotations,
            )
        if rgba is None:
            return empty_pil_img(*rast.shape[::-1]), Image.fromarray(data_mask)
        return Image.fromarray(rgba).quantize(64), Image.fromarray(data_mask)
    else:
        # Contourf
        assert len(rasters) == 1
        rast, nd_val = rasters[0]
        nd_mask = rast == nd_val
        if style_def.extra_nd_val is not None:
            nd_mask = np.logical_or(nd_mask, rast == style_def.extra_nd_val)
        if verbose:
            logger.info(
                "Raster stats: %.1f %.1f %.1f",
                rast.min(),
                rast.max(),
                rast.mean(),
            )
        rast = style_def.colormap.array_to_colors(rast, nd_mask=nd_mask)
    img = Image.fromarray(rast)
    img.putpalette(style_def.colormap.pallette, rawmode="RGBA")
    return img, Image.fromarray(~nd_mask)
