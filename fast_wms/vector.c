#include <math.h>
#include <stdio.h>

#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define ABS(x) ((x) < 0 ? -(x) : (x))
#define SQUARE(x) (x) * (x)
#define DOT(x, y) (x[0] * y[0] + x[1] * y[1])
#define MEPS (-1e-9)
#define PEPS (1 + 1e-9)
#define DET(x, y) (x[0] * y[1] - x[1] * y[0])

/* Internal declarations*/
static double d_p_line(double *p1, double *p2, double *p3);
static int draw_lines(unsigned char *img, double *lines, unsigned char val, unsigned char border, int nrows, int ncols, double buf, int n_lines);
static int draw_circle(unsigned char *img, double rad, int ci, int cj, unsigned char val, unsigned char border, int nrows, int ncols);
static int do_lines_intersect(double *p1, double *p2, double *p3, double *p4);
static int p_in_poly(double *p_in, double *bounds, double *lines, int nlines);
int fill_polygon(unsigned char *img, double *lines, unsigned char val, int nlines, int nrows, int ncols);
static int affine_transform(double *in, double *out, double dx, double dy, double angle, double scale, int n);
static int to_image_coords(double *in, double *out, double ci, double cj, int nrows, int n);
void masked_convolute(double *dem, double *out, double *kernel, char *mask, int nrows, int ncols, int ksize);

/* End internal declarations */

/*returns squared distance*/
static double d_p_line(double *p1, double *p2, double *p3)
{
    /* p1 is the point, p2, p3 - the line*/
    double p[2], v[2], dot1, dot2;
    p[0] = p1[0] - p2[0];
    p[1] = p1[1] - p2[1];
    v[0] = p3[0] - p2[0];
    v[1] = p3[1] - p2[1];
    dot1 = DOT(p, v);
    if (dot1 < 0)
    {
        /*puts("In da start!");*/
        return DOT(p, p);
    }
    dot2 = DOT(v, v);
    if (dot1 < dot2)
    {
        /*puts("Yep in da middle!");*/
        dot1 /= dot2;
        v[0] = p[0] - v[0] * dot1;
        v[1] = p[1] - v[1] * dot1;
        return DOT(v, v);
    }
    /*puts("in da end");*/
    v[0] = p3[0] - p1[0];
    v[1] = p3[1] - p1[1];
    return DOT(v, v);
    /*<x,v> v /<v,v> ,, |<x,v> v /<v,v>|^2 < <v,v> <-> <x,y> < <v,v> */
}

static int draw_lines(unsigned char *img, double *lines, unsigned char val, unsigned char border, int nrows, int ncols, double buf, int n_lines)
{
    /* p1, p2 in image coords (row, col) */
    int i, j, k, i1 = nrows, i2 = -1, j1 = ncols, j2 = -1;
    double p[2], dist, d, buf2, buf3;
    unsigned char outval;
    buf2 = SQUARE(buf);
    buf3 = SQUARE(buf - 1);
    for (i = 0; i < 2 * n_lines; i++)
    {
        i1 = MIN(i1, (int)(lines[2 * i] - buf));
        j1 = MIN(j1, (int)(lines[2 * i + 1] - buf));
        i2 = MAX(i2, (int)(lines[2 * i] + buf));
        j2 = MAX(j2, (int)(lines[2 * i + 1] + buf));
    }
    i1 = MAX(0, i1);
    i2 = MIN(nrows - 1, i2);
    j1 = MAX(0, j1);
    j2 = MIN(ncols - 1, j2);
    for (i = i1; i <= i2; i++)
    {
        p[0] = (i + 0.5);
        for (j = j1; j <= j2; j++)
        {
            p[1] = (j + 0.5);
            dist = -1;
            for (k = 0; k < n_lines; k++)
            {
                d = d_p_line(p, lines + 4 * k, lines + 4 * k + 2);
                if (dist < 0)
                    dist = d;
                else
                    dist = MIN(dist, d);
            }
            if (dist <= buf2)
            {
                outval = (dist <= buf3) ? val : border;
                img[i * ncols + j] = outval;
            }
        }
    }
    return 0;
}

static int draw_circle(unsigned char *img, double rad, int ci, int cj, unsigned char val, unsigned char border, int nrows, int ncols)
{
    int i, j, i1, i2, j1, j2;
    double dist, rad2, rad3;
    unsigned char outval;
    i1 = (int)MAX(0, ci - rad);
    j1 = (int)MAX(0, cj - rad);
    i2 = (int)MIN(nrows - 1, ci + rad);
    j2 = (int)MIN(ncols - 1, cj + rad);
    rad2 = rad * rad;
    rad3 = SQUARE(rad - 1);
    for (i = i1; i <= i2; i++)
    {
        for (j = j1; j <= j2; j++)
        {
            dist = SQUARE(j - cj) + SQUARE(i - ci);
            if (dist <= rad2)
            {
                outval = (dist <= rad3) ? val : border;
                img[i * ncols + j] = outval;
            }
        }
    }
}

static int do_lines_intersect(double *p1, double *p2, double *p3, double *p4)
{
    double v1[2], v2[2], v3[3], st[2], D;
    int i;
    for (i = 0; i < 2; i++)
    {
        v1[i] = p2[i] - p1[i];
        v2[i] = p3[i] - p4[i];
        v3[i] = p3[i] - p1[i];
    }
    D = DET(v1, v2);
    if (ABS(D) < 1e-10)
    {
        /* lines are almost parallel*/
        return 0;
    }
    st[0] = (v2[1] * v3[0] - v2[0] * v3[1]) / D;
    st[1] = (-v1[1] * v3[0] + v1[0] * v3[1]) / D;

    if (st[0] > MEPS && st[0] < PEPS && st[1] > MEPS && st[1] < PEPS)
        return 1;

    return 0;
}

static int p_in_poly(double *p_in, double *bounds, double *lines, int nlines)
{
    /*  lines are as otherwise (p1, p2, p2, p3, p3, p4, ... pn -1, p1) etc*/
    unsigned int i, n_hits;

    double p_end[2];

    /* printf("Bounds %.3f %.3f %.3f %.3f\n",bounds[0],bounds[1],bounds[2],bounds[3]);*/

    if (p_in[0] < bounds[0] || p_in[0] > bounds[1] || p_in[1] < bounds[2] || p_in[1] > bounds[3])
    {
        // printf("point: %.2f, %2f, bd: %.1f, %.1f, %.1f, %.1f\n", p_in[0], p_in[1], bounds[0], bounds[1], bounds[2], bounds[3]);
        return 0;
    }
    /*avoid parallel lines!*/
    p_end[1] = p_in[1] + 8.1234;
    p_end[0] = bounds[1] + 10; /*almost an infinite ray :-) */
    n_hits = 0;
    /*printf("p_in: %.2f %.2f\n",p_in[2*i],p_in[2*i+1]);*/
    for (i = 0; i < nlines; i++)
    {
        n_hits += do_lines_intersect(p_in, p_end, lines + 4 * i, lines + 4 * i + 2);
    }
    n_hits = n_hits % 2;
    // printf("nhits: %d, nlines: %d\n", n_hits, nlines);
    return n_hits;
}

int fill_polygon(unsigned char *img, double *lines, unsigned char val, int nlines, int nrows, int ncols)
{
    int i, j, i1, i2, j1, j2;
    double bounds[4], p[2]; /*x1,x2,y1,y2*/
    bounds[0] = lines[0];
    bounds[1] = lines[0];
    bounds[2] = lines[1];
    bounds[3] = lines[1];
    /* Because the lines close and repeat last point as first in next, we only need to consider first point*/
    for (i = 0; i < nlines; i++)
    {
        bounds[0] = MIN(bounds[0], lines[4 * i]);
        bounds[1] = MAX(bounds[1], lines[4 * i]);
        bounds[2] = MIN(bounds[2], lines[4 * i + 1]);
        bounds[3] = MAX(bounds[3], lines[4 * i + 1]);
    }
    i1 = (int)MAX(0, bounds[0]);
    i2 = (int)MIN(nrows - 1, bounds[1]);
    j1 = (int)MAX(0, bounds[2]);
    j2 = (int)MIN(ncols - 1, bounds[3]);

    for (i = i1; i <= i2; i++)
    {
        for (j = j1; j <= j2; j++)
        {
            p[0] = (i + 0.5);
            p[1] = (j + 0.5);
            // printf("%d, %d : %d, %d  %d, %d\n", i, j, i1, i2, j1, j2);
            if (p_in_poly(p, bounds, lines, nlines))
            {
                img[i * ncols + j] = val;
            }
        }
    }
}

static int affine_transform(double *in, double *out, double dx, double dy, double angle, double scale, int n)
{
    /* in and out are arrays of shape (n, 2) */
    int i;
    double cv, sv, x, y;
    cv = cos(angle);
    sv = sin(angle);
    for (i = 0; i < n; i++)
    {
        x = in[2 * i] - dx;
        y = in[2 * i + 1] - dy;
        out[2 * i] = scale * (x * cv - y * sv);
        out[2 * i + 1] = scale * (x * sv + y * cv);
        // printf("x, y: %.2f, %.2f\n", out[2 * i], out[2 * i + 1]);
    }
    return 0;
}

static int to_image_coords(double *in, double *out, double ci, double cj, int nrows, int n)
{
    /* in 'real' coords with x along cols and y 'up' (decreaing i)
       out will be in image coords as (i, j) (down, right)
    */
    int i;
    for (i = 0; i < n; i++)
    {
        out[2 * i + 1] = (in[2 * i] + cj);
        out[2 * i] = (ci - in[2 * i + 1]);
        // printf("x, y: %.2f, %.2f, i, j: %d, %d\n", in[2 * i], in[2 * i + 1], out[2 * i], out[2 * i + 1]);
    }
    return 0;
}

int draw_vector_field(
    double *angles,
    unsigned char *colors,
    unsigned char *out,
    // Vector pointing east (a number of lines in 'real' coords)
    double *lines,
    unsigned char transparent_val,
    double zero_val,
    unsigned char edge_val,
    int n_lines,
    int nrows,
    int ncols,
    // int vect_pix_size,
    // int spacing,
    int npix,
    double border_thickness,
    unsigned char do_fill)
{
    int i, j, k, ni, nj, cur_i, cur_j, ci, cj, rem_i, rem_j;
    double lines_out[4 * n_lines];
    double image_lines_out[4 * n_lines];
    // npix = vect_pix_size + spacing;
    ni = nrows / npix;
    nj = ncols / npix;
    rem_i = (nrows - npix * ni) / 2;
    rem_j = (ncols - npix * nj) / 2;
    // printf("ni: %d, nj: %d, spacing: %d, remi: %d, remj: %d\n", ni, nj, spacing, rem_i, rem_j);
    /* Calculate how many copies we need */
    /* Then loop, rotate and copy */
    for (i = 0; i < ni; i++)
    {
        cur_i = i * npix + rem_i;
        ci = cur_i + npix / 2;
        for (j = 0; j < nj; j++)
        {
            int color;
            double angle;
            cur_j = j * npix + rem_j;
            cj = cur_j + npix / 2;
            color = colors[ci * ncols + cj];
            // transparent_val means NO_DATA
            // zero val should draw a 'zero' vector with no rotaton
            if (color != transparent_val)
            {
                /* TODO: Special for zero val  */
                angle = angles[ci * ncols + cj];
                if (angle == zero_val)
                {
                    /* vanishing vector field */
                    draw_circle(out, 6.0, ci, cj, color, edge_val, nrows, ncols);
                }
                else
                {
                    // printf("Drawing at %d, %d, angle: %.2f, color: %d\n", ci, cj, angle, color);
                    affine_transform(lines, lines_out, 0, 0, angle, 1, 2 * n_lines);
                    to_image_coords(lines_out, image_lines_out, ci + 0.5, cj + 0.5, nrows, 2 * n_lines);
                    if (do_fill)
                    {
                        fill_polygon(out, image_lines_out, color, n_lines, nrows, ncols);
                    }
                    draw_lines(out, image_lines_out, edge_val, edge_val, nrows, ncols, border_thickness, n_lines);
                }
            }
        }
    }

    return 0;
}

void masked_convolute(double *dem, double *out, double *kernel, char *mask, int nrows, int ncols, int ksize)
{
    /*consider what to do about nd_vals - should probably be handled by caller */
    int i, j, i1, i2, j1, j2, m, n, ind1, ind2, filter_rad;
    double v, weight, total_weight;
    filter_rad = ksize / 2;
    for (i = 0; i < nrows; i++)
    {
        for (j = 0; j < ncols; j++)
        {
            ind1 = i * ncols + j;
            if (!mask[ind1])
            {
                /* Just copy */
                out[ind1] = dem[ind1];
                continue;
            }

            v = 0.0;
            total_weight = 0.0;
            for (m = 0; m < ksize; m++)
            {
                for (n = 0; n < ksize; n++)
                {
                    i1 = MIN(MAX(i - filter_rad + m, 0), nrows - 1);
                    j1 = MIN(MAX(j - filter_rad + n, 0), ncols - 1);
                    ind2 = i1 * ncols + j1;
                    if (mask[ind2])
                    {
                        weight = kernel[m * ksize + n];
                        v += weight * dem[ind2];
                        total_weight += weight;
                    }
                }
            }
            /*must be at least one used - well check anyways!*/
            if (total_weight > 0)
            {
                out[ind1] = v / total_weight;
            }
            else
            {
                out[ind1] = dem[ind1];
            }
        }
    }
}
