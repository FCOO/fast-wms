"""
Test the docker image from 'outside'.
Requires specific data files in data subdir
TODO: Add smallish data to repo for test
"""

import time
import io
import xml.etree.ElementTree as ET
import typing as T

import requests
from PIL import Image
import mapbox_vector_tile as mvt

x1, y1 = 1048725, 7327644
x2 = x1 + 100_000
y2 = y1 + 100_000

WMS_URLS = (
    # Contours
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetMap&version=1.3.0&layers=pressure&styles=plot_method%3Dcontour%3Blegend%3DSeaLevelPressure_hPa_interval4_1.0%3Bgaussian_filter%3D5&format=image%2Fpng&transparent=TRUE&cmap=SeaLevelPressure_hPa_interval4_1.0&width=512&height=512&time=2024-02-12T01%3A00%3A00.000Z&crs=EPSG%3A3857&bbox=1048725,7327644,1148725,7427644",
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetMap&version=1.3.0&layers=temp&styles=plot_method%3Dcontourf&cmap=AirTempGlobal_C_BWYR_16colors_1.0&format=image%2Fpng&transparent=TRUE&cmap=SeaLevelPressure_hPa_interval4_1.0&width=512&height=512&time=2024-02-12T01%3A00%3A00.000Z&crs=EPSG%3A3857&bbox=1048725,7327644,1148725,7427644",
    # Barbs
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetMap&version=1.3.0&layers=eastward_wind:northward_wind&styles=plot_method%3Dblack_arrowbarbs%3Bvector_spacing%3D80%3Bvector_offset%3D20&format=image%2Fpng&transparent=TRUE&cmap=Wind_ms_BGYRP_11colors_1.1&width=512&height=512&time=2024-02-12T01%3A00%3A00.000Z&crs=EPSG%3A3857&bbox=1048725,7327644,1148725,7427644",
    # Impacts
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetMap&version=1.3.0&layers=temp,pressure&styles=plot_method%3Dcontourf%3Blegend=Green_Red_3colors,plot_method%3Dcontourf%3Blegend=Green_Red_3colors&format=image%2Fpng&expr={%22temp%22:[-200,150],%22pressure%22:[8,11000]}&transparent=TRUE&width=512&height=512&time=2024-02-12T01%3A00%3A00.000Z&crs=EPSG%3A3857&bbox=1048725,7327644,1148725,7427644",
)

COLORBAR_URLS = (
    # Colorbar
    "http://localhost:8000/webmap/v3/data/data.nc.wms?request=GetColorbar&styles=horizontal%2Cnolabel&cmap=AirVisibility_km_RYG_11colors",
)

CAPABILITIES_URLS = (
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetCapabilities",
)

METADATA_URLS = (
    "http://localhost:8000/webmap/v3/data/data.nc.wms?SERVICE=WMS&REQUEST=GetMetadata&VERSION=1.3.0&ITEMS=epoch%2Clast_modified%2Clong_name%2Cunits%2Cbounds%2Ctime%2Clevels&LAYERS=temp",
)

MVT_URLS = (
    "http://localhost:8000/webmap/v3/data/data.nc.mvt?time=2024-02-12T01:00:00.000Z&z=8&x=135&y=79&variables=eastward_wind:northward_wind&dim=0",
)

WMS_EXCEPTION_URLS = (
    "http://localhost:8000/webmap/v3/data/data.nc.wms?service=WMS&request=GetMap",
)


def get_img(url: str) -> Image:
    t1 = time.perf_counter()
    r = requests.get(url)
    t2 = time.perf_counter()
    assert r.status_code == 200
    ds = url.split("/")[-2].split("?")[0]
    print("ds: %s, time: %.1f ms" % (ds, (t2 - t1) * 1e3))
    content = io.BytesIO(r.content)
    img = Image.open(content)
    return img


def get_mvt(url: str) -> dict[str, T.Any]:
    t1 = time.perf_counter()
    r = requests.get(url)
    t2 = time.perf_counter()
    assert r.status_code == 200
    ds = url.split("/")[-2].split("?")[0]
    print("ds: %s, time: %.1f ms" % (ds, (t2 - t1) * 1e3))
    tile = mvt.decode(r.content)
    return tile


def get_xml(
    url: str,
    status_code: int,
    headers: T.Dict[str, str] = None,
    return_text: bool = False,
) -> ET.Element | str:
    t1 = time.perf_counter()
    r = requests.get(url, headers=headers)
    t2 = time.perf_counter()
    assert r.status_code == status_code
    ds = url.split("/")[-2].split("?")[0]
    print("ds: %s, time: %.1f ms" % (ds, (t2 - t1) * 1e3))
    if return_text:
        return r.text
    tree = ET.fromstring(r.text)
    return tree


def get_json(url: str) -> dict[str, T.Any]:
    t1 = time.perf_counter()
    r = requests.get(url)
    t2 = time.perf_counter()
    assert r.status_code == 200
    ds = url.split("/")[-2].split("?")[0]
    print("ds: %s, time: %.1f ms" % (ds, (t2 - t1) * 1e3))
    return r.json()


def test_getmap() -> None:
    print("+" * 80)
    print("Testing GetMap")
    for url in WMS_URLS:
        img = get_img(url)
        assert img.size == (512, 512)


def test_getcolorbar() -> None:
    print("+" * 80)
    print("Testing GetColorbar")
    for url in COLORBAR_URLS:
        img = get_img(url)


def test_mvt() -> None:
    print("+" * 80)
    print("Testing MVT")
    for url in MVT_URLS:
        tile = get_mvt(url)
        for k, lyr in tile.items():
            print("Layer: %s" % k)
            assert "features" in lyr
            assert lyr["extent"] == 4096


def test_capabilities() -> None:
    print("+" * 80)
    print("Testing GetCapabilities")
    for url in CAPABILITIES_URLS:
        elm = get_xml(url, 200)
        assert elm.tag == "{http://www.opengis.net/wms}WMS_Capabilities"


def test_capabilities2() -> None:
    print("+" * 80)
    print("Testing GetCapabilities with x-forwarded-host")
    for url in CAPABILITIES_URLS:
        text = get_xml(
            url, 200, headers={"x-forwarded-host": "hej.mig.dk"}, return_text=True
        )
        assert "http://hej.mig.dk" in text


def test_wms_exception() -> None:
    print("+" * 80)
    print("Testing WMSException")
    for url in WMS_EXCEPTION_URLS:
        elm = get_xml(url, 400)
        assert elm.tag == "{http://www.opengis.net/ogc}ServiceExceptionReport"


def test_getmetadata() -> None:
    print("+" * 80)
    print("Testing GetMetadata")
    for url in METADATA_URLS:
        data = get_json(url)
        assert "epoch" in data
