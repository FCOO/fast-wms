import datetime

import xarray as xr
import numpy as np


def in_mem_dataset(nrows: int = 100, ntimes: int = 10) -> xr.Dataset:
    ncols = nrows
    temp = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    temp0 = np.repeat(np.linspace(-10, 10, ncols).reshape((1, ncols)), nrows, axis=0)
    press = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    press0 = np.repeat(np.linspace(900, 1010, ncols).reshape((1, ncols)), nrows, axis=0)
    uwind = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    vwind = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    base_wind = np.ones((nrows, ncols), dtype=np.float32) * 10
    for i in range(ntimes):
        f1 = 1 - (i / ntimes)
        f2 = 1 - f1
        ang = np.pi / ntimes * i
        temp[i, :, :] = f1 * temp0 + f2 * np.transpose(temp0)
        press[i, :, :] = f1 * press0 + f2 * np.transpose(press0)
        uwind[i, :, :] = base_wind * np.cos(ang)
        vwind[i, :, :] = base_wind * np.sin(ang)

    
    ds = xr.Dataset(
        coords={
            "lon": np.linspace(8, 16, ncols),
            "lat": np.linspace(50, 58, nrows),
            "time": [
                datetime.datetime(2024, 2, 12) + datetime.timedelta(hours=i)
                for i in range(ntimes)
            ],
        },
        data_vars={
            "temp": xr.Variable(
                dims=("time", "lat", "lon"),
                data=temp,
                attrs={
                    "standard_name": "air_temperature",
                    "units": "degC",
                    "long_name": "2m air temperature",
                },
            ),
            "eastward_wind": xr.Variable(
                dims=("time", "lat", "lon"),
                data=uwind,
                attrs={
                    "standard_name": "eastward_wind",
                    "units": "m/s",
                    "long_name": "10 metre U wind component",
                },
            ),
            "northward_wind": xr.Variable(
                dims=("time", "lat", "lon"),
                data=vwind,
                attrs={
                    "standard_name": "northward_wind",
                    "units": "m/s",
                    "long_name": "10 metre V wind component",
                },
            ),
            "pressure": xr.Variable(
                dims=("time", "lat", "lon"),
                data=press,
                attrs={
                    "standard_name": "surface_air_pressure",
                    "units": "hPa",
                    "long_name": "Mean sea level pressure",
                },
            ),
        },
    )
    return ds


def in_mem_dataset2(ncols: int, nrows: int, lon0: float, lon1: float, lat0:float, lat1: float, ntimes: int = 10) -> xr.Dataset:
    temp = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    temp0 = np.repeat(np.linspace(-10, 10, ncols).reshape((1, ncols)), nrows, axis=0)
    press = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    press0 = np.repeat(np.linspace(900, 1010, ncols).reshape((1, ncols)), nrows, axis=0)
    uwind = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    vwind = np.empty((ntimes, nrows, ncols), dtype=np.float32)
    base_wind = np.ones((nrows, ncols), dtype=np.float32) * 10
    for i in range(ntimes):
        f1 = 1 - (i / ntimes)
        f2 = 1 - f1
        ang = np.pi / ntimes * i
        temp[i, :, :] = f1 * temp0 + f2 * temp0[::-1, ::-1]
        press[i, :, :] = f1 * press0 + f2 * press0[::-1, ::-1]
        uwind[i, :, :] = base_wind * np.cos(ang)
        vwind[i, :, :] = base_wind * np.sin(ang)

    
    ds = xr.Dataset(
        coords={
            "lon": np.linspace(lon0, lon1, ncols),
            "lat": np.linspace(lat0, lat1, nrows),
            "time": [
                datetime.datetime(2024, 2, 12) + datetime.timedelta(hours=i)
                for i in range(ntimes)
            ],
        },
        data_vars={
            "temp": xr.Variable(
                dims=("time", "lat", "lon"),
                data=temp,
                attrs={
                    "standard_name": "air_temperature",
                    "units": "degC",
                    "long_name": "2m air temperature",
                },
            ),
            "eastward_wind": xr.Variable(
                dims=("time", "lat", "lon"),
                data=uwind,
                attrs={
                    "standard_name": "eastward_wind",
                    "units": "m/s",
                    "long_name": "10 metre U wind component",
                },
            ),
            "northward_wind": xr.Variable(
                dims=("time", "lat", "lon"),
                data=vwind,
                attrs={
                    "standard_name": "northward_wind",
                    "units": "m/s",
                    "long_name": "10 metre V wind component",
                },
            ),
            "pressure": xr.Variable(
                dims=("time", "lat", "lon"),
                data=press,
                attrs={
                    "standard_name": "surface_air_pressure",
                    "units": "hPa",
                    "long_name": "Mean sea level pressure",
                },
            ),
        },
    )
    ds.lon.attrs = {"standard_name": "degrees_east"}
    ds.lat.attrs = {"standard_name": "degrees_north"}
    ds.time.attrs = {"standard_name": "time", "long_name": "time"}
    return ds
